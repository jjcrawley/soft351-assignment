#include "Boid.h"
#include "Gameobject.h"

using namespace DirectX;

Boid::Boid()
{
	float randomVelocity = -1.0f + (static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * 2.0f;
	m_currentVelocity = XMVectorSet(randomVelocity, 0.0f, randomVelocity, 0.0f);
	m_currentAcceleration = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	m_position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_scale = XMFLOAT3(1.0f, 1.0f, 1.0f);
	m_boidMaxSpeed = 10.0f;
	m_boidMinSpeed = 5.0f;
	m_rotation = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
}

void Boid::Init(XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale)
{
	m_position = position;
	m_rotation = XMQuaternionRotationRollPitchYaw(rotation.x, rotation.y, rotation.z);
	m_scale = scale;
}

void Boid::Update()
{
	Flock();
}

void Boid::Move(float delta)
{
	XMVECTOR myPos = XMLoadFloat3(&m_position);

	//m_currentVelocity += (m_currentAcceleration * delta);
	//m_currentVelocity = m_currentAcceleration * delta;
	m_currentVelocity += m_currentAcceleration * 0.05f;

	float currentSpeed = XMVectorGetX(XMVector3Length(m_currentVelocity));

	if (currentSpeed > m_boidMaxSpeed)
	{
		m_currentVelocity = XMVector3Normalize(m_currentVelocity) * m_boidMaxSpeed;
	}
	else if (currentSpeed < m_boidMinSpeed)
	{
		m_currentVelocity = XMVector3Normalize(m_currentVelocity) * m_boidMinSpeed;
	}
	
	//update the position using the current velocity
	XMStoreFloat3(&m_position, (m_currentVelocity * delta + myPos));

	float yRotation = atan2(XMVectorGetX(m_currentVelocity), XMVectorGetZ(m_currentVelocity));

	m_rotation = XMQuaternionRotationRollPitchYaw(0.0f, yRotation, 0.0f);
}

void Boid::SetBOIDArray(std::vector<Boid*>* boid)
{
	m_boids = boid;
}

void Boid::SetSpeed(float speed)
{
	m_boidMaxSpeed = speed;
}

void Boid::ClampPosition(RECT region)
{
	if (m_position.x < region.left)
	{
		m_position.x = (float)region.right;
	}
	else if (m_position.x > region.right)
	{
		m_position.x = (float) region.left;
	}

	if (m_position.z < region.bottom)
	{
		m_position.z = (float) region.top;
	}
	else if (m_position.z > region.top)
	{
		m_position.z = (float) region.bottom;
	}
}

void Boid::SetObjectToAvoid(Gameobject* object)
{
	m_avoidObject = object;
}

XMFLOAT3 Boid::GetPosition()
{
	return m_position;
}

DirectX::XMVECTOR Boid::GetRotation()
{
	return m_rotation;
}

XMMATRIX Boid::GetWorldMatrix()
{
	XMMATRIX worldMatrix;

	XMMATRIX scale = XMMatrixScaling(m_scale.x, m_scale.y, m_scale.z);
	XMMATRIX rotation = XMMatrixRotationQuaternion(m_rotation);
	XMMATRIX position = XMMatrixTranslation(m_position.x, m_position.y, m_position.z);

	worldMatrix = scale * rotation * position;

	return worldMatrix;
}

void Boid::Flock()
{
	//this is the mean seperation for this boid
	//this represents the closeness of the boids to this one 
	//this can be thought of as a force that repels this boid from any boids that are too close
	XMVECTOR seperationMean = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	int seperationCount = 0;

	//this is the mean alignment for the boid
	//it represents the alignment to meet using the surrounding boids
	XMVECTOR alignmentMean = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	int alignmentCount = 0;

	//this represents how closely knit the flock is, the centre of all neighbouring boids
	XMVECTOR neighbourCentre = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	int cohesionCount = 0;

	//the desired seperation for the boid
	float desiredSeperation = SEP_DIST;
	//the distance at which this boid considers other boids to be neighbours
	float neighbourDistance = NEIGHBOUR_DIST;

	float sqrSeperationDist = pow(desiredSeperation, 2);
	float sqrNeighbourDistance = pow(neighbourDistance, 2);

	XMVECTOR myPos = XMLoadFloat3(&m_position);

	for (int i = 0; i < m_boids->size(); i++)
	{
		Boid* currentBoid = m_boids->at(i);

		if (currentBoid == this)
			continue;

		XMVECTOR tempPos = XMLoadFloat3(&currentBoid->m_position);
		
		float sqrDistance = XMVectorGetX(XMVector3LengthSq(tempPos - myPos));
		
		if (sqrDistance > 0.0f)
		{
			//if the current boid is considered a neighbour
			if (sqrDistance < sqrNeighbourDistance)
			{
				//update the alignment mean
				alignmentMean += currentBoid->m_currentVelocity;
				alignmentCount++;

				//update the cohesion mean, this is the mean position to aim for
				neighbourCentre += tempPos;
				cohesionCount++;
			}

			//if the boid is too close to the other boid
			if (sqrDistance < sqrSeperationDist)
			{
				//get the vector between the two positions, normalise it and then weight it using distance, smaller distance greater influence
				XMVECTOR tempDistVec = (myPos - tempPos);

				tempDistVec = XMVector3Normalize(tempDistVec) / sqrt(sqrDistance);
				//tempDistVec *= 3.0f;
				seperationMean += tempDistVec;
				seperationCount++;
			}
		}
 	}

	//calculate actual means
	if (seperationCount > 0)
	{
		seperationMean /= (float) seperationCount;
	}
	
	if (alignmentCount > 0)
	{
		alignmentMean /= (float) alignmentCount;
		
		float turnSpeed = MAX_FORCE;

		if (turnSpeed < XMVectorGetX(XMVector3Length(alignmentMean)))
		{
			alignmentMean = XMVector3Normalize(alignmentMean) * MAX_FORCE;
		}
	}

	//get the actual mean, else use the boid's position
	if (cohesionCount > 0)
	{
		neighbourCentre /= (float) cohesionCount;
		neighbourCentre = SteerTo(neighbourCentre);
	}	
	
	XMVECTOR total = alignmentMean * 1.0f + seperationMean * 2.0f + neighbourCentre * 1.0f;
	
	if (m_avoidObject != nullptr)
	{
		total += AvoidObject() * 10.0f;
	}

	m_currentAcceleration = total;	
	m_currentAcceleration = XMVectorSetY(m_currentAcceleration, 0.0f);
}

XMVECTOR Boid::SteerTo(XMVECTOR target)
{
	//the returned vector, the steering vector
	XMVECTOR steer = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
		
	//get the vector between the target and the current position
	XMVECTOR desiredSteering = target - XMLoadFloat3(&m_position);
	steer = desiredSteering;

	float magnitude = XMVectorGetX(XMVector3Length(desiredSteering));

	//if the magnitude is greater than 0, should be
	if (magnitude > 0.0f)
	{
		//normalise the steering, only interested in main components
		desiredSteering = XMVector3Normalize(desiredSteering);

		//useful for slowing down behaviour when near the target
		if (magnitude < 10.0f)
		{
			desiredSteering *= m_boidMaxSpeed * magnitude / 10.0f;
		}
		else
		{
			//multiply by speed, makes a speed vector aka: velocity
			desiredSteering *= m_boidMaxSpeed;
		}

		//get the difference between the current and desired velocities
		steer = desiredSteering - m_currentVelocity;

		//scale the steering vector
		float turnSpeed = MAX_FORCE;

		if (turnSpeed < XMVectorGetX(XMVector3Length(steer)))
		{
			steer = XMVector3Normalize(steer) * MAX_FORCE;
		}
	}

	return steer;
}

XMVECTOR Boid::AvoidObject()
{
	XMVECTOR steer = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);

	XMFLOAT3 pos = m_avoidObject->GetPosition();

	XMVECTOR avoidPos = XMLoadFloat3(&pos);

	XMVECTOR myPos = XMLoadFloat3(&m_position);

	XMVECTOR difference = avoidPos - myPos;

	float length = XMVectorGetX(XMVector3Length(difference));
	
	length = length < 0.0f ? 0.01f : length;

	if (length < 20.0f)
	{
		steer = XMVector3Normalize(difference);

		steer /= pow(length,2) * -1.0f;
	}	

	return steer;
}