#pragma once
#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>

#define MAX_SPEED 5.0f;
#define SEP_DIST 6.0f;
#define NEIGHBOUR_DIST 10.0f;
#define MAX_FORCE 0.05f;
#define SEP_WEIGHT 0.2f;
#define ALIGN_WEIGHT 0.2f;
#define COH_WEIGHT 0.2f;

#ifndef BOID

#define BOID

class Gameobject;

class Boid
{
public:
	Boid();
	void Init(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 rotation, DirectX::XMFLOAT3 scale);
	void Update();
	void Move(float delta);
	void SetBOIDArray(std::vector<Boid*>* boid);
	void SetSpeed(float speed);
	void ClampPosition(RECT region);
	void SetObjectToAvoid(Gameobject* position);
	DirectX::XMFLOAT3 GetPosition();
	DirectX::XMVECTOR GetRotation();
	DirectX::XMMATRIX GetWorldMatrix();
private:
	DirectX::XMFLOAT3 m_position;
	float m_boidMaxSpeed;
	DirectX::XMVECTOR m_rotation;
	DirectX::XMFLOAT3 m_scale;
	float m_boidMinSpeed;
	DirectX::XMVECTOR m_currentVelocity;
	DirectX::XMVECTOR m_currentAcceleration;
	std::vector<Boid*>* m_boids;	
	Gameobject* m_avoidObject;
	void Flock();
	DirectX::XMVECTOR SteerTo(DirectX::XMVECTOR target);
	DirectX::XMVECTOR AvoidObject();
};

#endif // !BOID