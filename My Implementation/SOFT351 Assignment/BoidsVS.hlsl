
//**************************************************************************//
// No longer used; it is here to prove a point which is that you can have	//
// many constant buffers.													//
//**************************************************************************//
cbuffer cbChangeOnResize : register(b0)
{
	matrix Projection;
};

//**************************************************************************//
// Matrices constant buffer.												//
//**************************************************************************//
cbuffer cbChangesEveryFrame : register(b1)
{
	matrix MatWorld;
	matrix MatWorldViewProjection;
};

cbuffer cbPerBOIDS : register(b2)
{
	matrix boids[50];
};

struct VS_INPUT
{
	float4 Pos       : POSITION;
	float3 VecNormal : NORMAL;
	float2 Tex       : TEXCOORD;
	float3 Tangent   : TANGENT;
	//	float3 InstancePos : INSTANCEPOS;
	uint InstanceID  : SV_InstanceID;
};

struct VS_OUTPUT
{
	float4 Pos		 : SV_POSITION;
	float4 WorldPos  : POSITION;
	float3 VecNormal : NORMAL;
	float3 Tangent   : TANGENT;
	float2 Tex		 : TEXCOORD;
};

VS_OUTPUT VS_InstanceBOIDS(VS_INPUT input)
{
	VS_OUTPUT output;
	
	input.Pos = mul(input.Pos, boids[input.InstanceID]);
		
	output.Pos = mul(input.Pos, MatWorldViewProjection);

	output.WorldPos = input.Pos; 

	output.VecNormal = mul(input.VecNormal, (float3x3)boids[input.InstanceID]);
	output.VecNormal = normalize(output.VecNormal);

	output.Tangent = mul(input.Tangent, (float3x3)boids[input.InstanceID]);
	output.Tangent = normalize(output.Tangent);

	output.Tex = input.Tex;

	return output;
}