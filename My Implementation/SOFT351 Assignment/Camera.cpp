#include "Camera.h"
#include "Input.h"
#include "Gameobject.h"
#include <DirectXPackedVector.h>

using namespace DirectX;

void Camera::Update(float delta)
{
	SetupViewMatrix();
			
	SetupFrustumPlanes();
}

XMMATRIX Camera::GetViewMatrix()
{
	XMMATRIX matView = m_matView;

	return matView;
}

void Camera::SetPosition(float x, float y, float z)
{
	m_position.x = x;
	m_position.y = y;
	m_position.z = z;	
}

void Camera::SetFieldOfView(float farPlane, float nearPlane, float aspectRatio, float fieldOfView)
{
	m_farPlane = farPlane;
	m_nearPlane = nearPlane;
	m_aspectRatio = aspectRatio;
	m_fieldOfView = fieldOfView;

	m_matProjection = XMMatrixPerspectiveFovLH(fieldOfView, aspectRatio, nearPlane, farPlane);
}

XMMATRIX Camera::GetViewProjectionMatrix()
{
	XMMATRIX matView = m_matView;
	XMMATRIX matProjection = m_matProjection;

	return matView * matProjection;
}

XMMATRIX Camera::GetProjectionMatrix()
{
	XMMATRIX matProjection = m_matProjection;

	return matProjection;
}

XMFLOAT3 Camera::GetPosition()
{
	return m_position;
}

bool Camera::CullObject(BoundingBox box)
{
	for (int i = 0; i < 6; i++)
	{
		XMFLOAT4 currentPlane = m_frustumPlanes[i];

		XMVECTOR planeNormal = XMVectorSet(currentPlane.x, currentPlane.y, currentPlane.z, 0.0f);

		float planeConstant = currentPlane.w;

		XMFLOAT3 axisVert;

		if (currentPlane.x < 0.0f)
		{
			axisVert.x = box.minPosition.x;
		}
		else
		{
			axisVert.x = box.maxPosition.x;
		}

		if (currentPlane.y < 0.0f)
		{
			axisVert.y = box.minPosition.y;
		}
		else
		{
			axisVert.y = box.maxPosition.y;
		}

		if (currentPlane.z < 0.0f)
		{
			axisVert.z = box.minPosition.z;
		}
		else
		{
			axisVert.z = box.maxPosition.z;
		}

		XMVECTOR angle;

		angle = XMVector3Dot(planeNormal, XMLoadFloat3(&axisVert));

		if (XMVectorGetX(angle) + planeConstant < 0.0f)
		{
			return true;
		}
	}

	return false;
}

bool Camera::CullObject(BoundingSphere sphere)
{
	int size = (int)m_frustumPlanes.size();

	for (int i = 0; i < size; i++)
	{
		XMFLOAT4 currentPlane = m_frustumPlanes[i];

		//this is the dot product between the plane and the point
		float distance = sphere.centre.x * currentPlane.x + sphere.centre.y * currentPlane.y + sphere.centre.z * currentPlane.z + currentPlane.w;

		//if the distance <= radius, equivalent to adding on the radius to distance and checking if its < 0
		if (distance <= -sphere.radius)
		{
			return true;
		}
	}

	return false;
}

void Camera::LookAt(XMFLOAT3 at)
{
	// Sets the camera rotation
	//m_at = at;

	XMVECTOR atVec = XMLoadFloat3(&at);
	XMVECTOR myPos = XMLoadFloat3(&m_position);

	XMVECTOR direction = atVec - myPos;
		
	LookInDirection(direction);
}
		
void Camera::LookInDirection(XMVECTOR direction)
{
	// Needs to calculate a new rotation for the camera
	/*m_lookDirection = direction;

	direction = XMVector3Normalize(direction);
	XMStoreFloat3(&m_rotation, direction);
*/
	direction = XMVector3Normalize(direction);

	// Calculate the pitch and the heading
	const float pitch = asinf(-XMVectorGetY(direction));

	// We avoid issues with gimbal lock by setting the heading to be 0 if pitch is PI / 2 radians, aka: 90 degrees
	const float heading = fabs(pitch) < XM_PIDIV2 ? atan2f(XMVectorGetX(direction), XMVectorGetZ(direction)) : 0.0f;

	m_rotation.z = 0.0f;
	m_rotation.y = heading;
	m_rotation.x = pitch;
}

XMVECTOR Camera::GetLookDirection()
{
	const float x = cosf(m_rotation.x) * sinf(m_rotation.y);	
	const float y = -sinf(m_rotation.x);
	const float z = cosf(m_rotation.x) * cosf(m_rotation.y);

	return XMVectorSet(x, y, z, 0.0f);
}

void Camera::SetupFrustumPlanes()
{
	XMMATRIX matView = m_matView;
	XMMATRIX matProjection = m_matProjection;

	XMMATRIX matViewProjection = matView * matProjection;

	std::vector<XMFLOAT4> tempPlanes;		

	XMFLOAT4 leftPlane;
	XMFLOAT4 rightPlane;
	XMFLOAT4 farPlane;
	XMFLOAT4 nearPlane;
	XMFLOAT4 topPlane;
	XMFLOAT4 bottomPlane;
	
	leftPlane.x = matViewProjection.r[0].m128_f32[3] + matViewProjection.r[0].m128_f32[0];
	leftPlane.y = matViewProjection.r[1].m128_f32[3] + matViewProjection.r[1].m128_f32[0];
	leftPlane.z = matViewProjection.r[2].m128_f32[3] + matViewProjection.r[2].m128_f32[0];
	leftPlane.w = matViewProjection.r[3].m128_f32[3] + matViewProjection.r[3].m128_f32[0];

	rightPlane.x = matViewProjection.r[0].m128_f32[3] - matViewProjection.r[0].m128_f32[0];
	rightPlane.y = matViewProjection.r[1].m128_f32[3] - matViewProjection.r[1].m128_f32[0];
	rightPlane.z = matViewProjection.r[2].m128_f32[3] - matViewProjection.r[2].m128_f32[0];
	rightPlane.w = matViewProjection.r[3].m128_f32[3] - matViewProjection.r[3].m128_f32[0];

	topPlane.x = matViewProjection.r[0].m128_f32[3] - matViewProjection.r[0].m128_f32[1];
	topPlane.y = matViewProjection.r[1].m128_f32[3] - matViewProjection.r[1].m128_f32[1];
	topPlane.z = matViewProjection.r[2].m128_f32[3] - matViewProjection.r[2].m128_f32[1];
	topPlane.w = matViewProjection.r[3].m128_f32[3] - matViewProjection.r[3].m128_f32[1];

	bottomPlane.x = matViewProjection.r[0].m128_f32[3] + matViewProjection.r[0].m128_f32[1];
	bottomPlane.y = matViewProjection.r[1].m128_f32[3] + matViewProjection.r[1].m128_f32[1];
	bottomPlane.z = matViewProjection.r[2].m128_f32[3] + matViewProjection.r[2].m128_f32[1];
	bottomPlane.w = matViewProjection.r[3].m128_f32[3] + matViewProjection.r[3].m128_f32[1];

	nearPlane.x = matViewProjection.r[0].m128_f32[2];
	nearPlane.y = matViewProjection.r[1].m128_f32[2];
	nearPlane.z = matViewProjection.r[2].m128_f32[2];
	nearPlane.w = matViewProjection.r[3].m128_f32[2];

	farPlane.x = matViewProjection.r[0].m128_f32[3] - matViewProjection.r[0].m128_f32[2];
	farPlane.y = matViewProjection.r[1].m128_f32[3] - matViewProjection.r[1].m128_f32[2];
	farPlane.z = matViewProjection.r[2].m128_f32[3] - matViewProjection.r[2].m128_f32[2];
	farPlane.w = matViewProjection.r[3].m128_f32[3] - matViewProjection.r[3].m128_f32[2];

	/*
	// Written using old xna math
	leftPlane.x = matViewProjection._14 + matViewProjection._11;
	leftPlane.y = matViewProjection._24 + matViewProjection._21;
	leftPlane.z = matViewProjection._34 + matViewProjection._31;
	leftPlane.w = matViewProjection._44 + matViewProjection._41;

	rightPlane.x = matViewProjection._14 - matViewProjection._11;
	rightPlane.y = matViewProjection._24 - matViewProjection._21;
	rightPlane.z = matViewProjection._34 - matViewProjection._31;
	rightPlane.w = matViewProjection._44 - matViewProjection._41;

	topPlane.x = matViewProjection._14 - matViewProjection._12;
	topPlane.y = matViewProjection._24 - matViewProjection._22;
	topPlane.z = matViewProjection._34 - matViewProjection._32;
	topPlane.w = matViewProjection._44 - matViewProjection._42;

	bottomPlane.x = matViewProjection._14 + matViewProjection._12;
	bottomPlane.y = matViewProjection._24 + matViewProjection._22;
	bottomPlane.z = matViewProjection._34 + matViewProjection._32;
	bottomPlane.w = matViewProjection._44 + matViewProjection._42;

	nearPlane.x = matViewProjection._13;
	nearPlane.y = matViewProjection._23;
	nearPlane.z = matViewProjection._33;
	nearPlane.w = matViewProjection._43;

	farPlane.x = matViewProjection._14 - matViewProjection._13;
	farPlane.y = matViewProjection._24 - matViewProjection._23;
	farPlane.z = matViewProjection._34 - matViewProjection._33;
	farPlane.w = matViewProjection._44 - matViewProjection._43;*/

	XMFLOAT4normalise(&leftPlane);
	XMFLOAT4normalise(&rightPlane);
	XMFLOAT4normalise(&topPlane);
	XMFLOAT4normalise(&bottomPlane);
	XMFLOAT4normalise(&nearPlane);
	XMFLOAT4normalise(&farPlane);

	tempPlanes.push_back(leftPlane);
	tempPlanes.push_back(rightPlane);
	tempPlanes.push_back(topPlane);
	tempPlanes.push_back(bottomPlane);
	tempPlanes.push_back(nearPlane);
	tempPlanes.push_back(farPlane);

	m_frustumPlanes = tempPlanes;
}

void Camera::SetupViewMatrix()
{
	XMVECTOR eye = XMVectorSet(m_position.x, m_position.y, m_position.z, 0);
	XMVECTOR up = XMVectorSet(m_upDirection.x, m_upDirection.y, m_upDirection.z, 0);
	
	XMVECTOR lookDirection = GetLookDirection();

	// Convert rotation into direction vector, use that
	XMMATRIX matView = XMMatrixLookToLH(eye, lookDirection, up);

	// Modify how this works, need to use the rotation to construct the new view matrix
	/*if (!m_useTarget)
	{
		XMVECTOR direction = XMVectorSet(XMVectorGetX(m_lookDirection), XMVectorGetY(m_lookDirection), XMVectorGetZ(m_lookDirection), 0);
		matView = XMMatrixLookToLH(eye, direction, up);
	}
	else
	{
		XMVECTOR at = XMVectorSet(m_at.x, m_at.y, m_at.z, 0);
		matView = XMMatrixLookAtLH(eye, at, up);
	}*/
	
	m_matView = matView;
}

void Camera::XMFLOAT4normalise(XMFLOAT4 *toNormalise)
{
	float magnitude = (toNormalise->x * toNormalise->x 
		             + toNormalise->y * toNormalise->y
					 + toNormalise->z * toNormalise->z);

	magnitude = sqrt(magnitude);
	toNormalise->x /= magnitude;	
	toNormalise->y /= magnitude;	
	toNormalise->z /= magnitude;
	toNormalise->w /= magnitude;
}