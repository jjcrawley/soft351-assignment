#pragma once
#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>

#ifndef CAMERA
#define CAMERA

struct BoundingBox;
struct BoundingSphere;

class Camera
{
public:
	virtual void Update(float delta);
	DirectX::XMMATRIX GetViewMatrix();
	void SetPosition(float x, float y, float z);	
	void SetFieldOfView(float farPlane, float nearPlane, float aspectRatio, float fieldOfView);
	DirectX::XMMATRIX GetViewProjectionMatrix();
	DirectX::XMMATRIX GetProjectionMatrix();
	DirectX::XMFLOAT3 GetPosition();
	bool CullObject(BoundingBox box);
	bool CullObject(BoundingSphere sphere);
protected:
	DirectX::XMFLOAT3 m_position = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	DirectX::XMFLOAT3 m_rotation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	DirectX::XMMATRIX m_matView;
	DirectX::XMMATRIX m_matProjection;
	float m_farPlane;
	float m_nearPlane;
	float m_fieldOfView;
	float m_aspectRatio;
	DirectX::XMFLOAT3 m_upDirection = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
	void LookAt(DirectX::XMFLOAT3 at);
	void LookInDirection(DirectX::XMVECTOR direction);
	DirectX::XMVECTOR GetLookDirection();
private:
	std::vector<DirectX::XMFLOAT4> m_frustumPlanes;
	void SetupFrustumPlanes();
	void SetupViewMatrix();
	void XMFLOAT4normalise(DirectX::XMFLOAT4* toNormalise);
};

#endif // !CAMERA