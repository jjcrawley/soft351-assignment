#include "Crawler.h"
#include "Gameobject.h"

using namespace DirectX;

Crawler::~Crawler()
{
	m_sky = nullptr;
	m_skyBoxInfo = nullptr;
}

void Crawler::Update(float delta)
{
	if (b_waiting)
	{
		m_currentTime += delta;

		if (m_currentTime > m_waitTime)
		{
			m_currentTime = 0.0f;
			b_waiting = false;

			GetNextTarget();
		}
		else
		{
			XMFLOAT3 skyboxPos = m_sky->GetPosition();

			XMFLOAT2 relativePos;

			relativePos.x = m_position.x + skyboxPos.x;
			relativePos.y = m_position.y + skyboxPos.z;
			
			m_skyBoxInfo->crawlerPos.x = relativePos.x;
			m_skyBoxInfo->crawlerPos.z = relativePos.y;

			return;
		}
	}

	Move(delta);
}

void Crawler::SetSky(Gameobject* sky)
{
	m_sky = sky;
}

void Crawler::SetSkyBoxInfo(CBSkybox* info)
{
	m_skyBoxInfo = info;
}

void Crawler::Move(float delta)
{
	XMFLOAT3 skyboxPos = m_sky->GetPosition();

	XMFLOAT2 relativePos;
	XMFLOAT2 relativeTargetPos;

	relativePos.x = m_position.x + skyboxPos.x;
	relativePos.y = m_position.y + skyboxPos.z;

	relativeTargetPos.x = m_target.x + skyboxPos.x;
	relativeTargetPos.y = m_target.y + skyboxPos.z;

	XMVECTOR targetPos = XMLoadFloat2(&relativeTargetPos);
	XMVECTOR position = XMLoadFloat2(&relativePos);

	XMVECTOR difference = targetPos - position;

	float distance = XMVectorGetX(XMVector2LengthSq(difference));

	difference = XMVector2Normalize(difference) * m_speed * delta;

	m_position.x += XMVectorGetX(difference);
	m_position.y += XMVectorGetY(difference);

	if (distance < pow(m_changeRadius, 2))
	{
		b_waiting = true;
	}

	m_skyBoxInfo->crawlerPos.x = relativePos.x;
	m_skyBoxInfo->crawlerPos.z = relativePos.y;
}

void Crawler::GetNextTarget()
{
	BoundingBox box = m_sky->GetBoundingBox();

	XMFLOAT2 limits;
	limits.x = abs(box.maxPosition.x - box.minPosition.x) / 2.0f;
	limits.y = abs(box.maxPosition.z - box.minPosition.z) / 2.0f;

	m_target.x = ((float) rand() / (float) RAND_MAX) * limits.x;
	m_target.y = ((float) rand() / (float) RAND_MAX) * limits.y;

	if (rand() % 2 == 0)
	{
		m_target.x *= -1.0f;
	}

	if (rand() % 2 == 0)
	{
		m_target.y *= -1.0f;
	}	
}