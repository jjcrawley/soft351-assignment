#pragma once
#include <d3d11.h>
#include <dxgi.h>
#include <DirectXMath.h>
#include "Renderer.h"

class Gameobject;

class Crawler
{
public:
	~Crawler();
	void Update(float delta);
	void SetSky(Gameobject* sky);
	void SetSkyBoxInfo(CBSkybox* info);
private:
	DirectX::XMFLOAT2 m_position = DirectX::XMFLOAT2(0, 0);
	DirectX::XMFLOAT2 m_target = DirectX::XMFLOAT2(0, 0);
	DirectX::XMFLOAT2 m_limits;
	float m_speed = 50.0f;
	float m_waitTime = 3.0f;
	float m_currentTime = 0.0f;
	bool b_waiting = true;
	bool b_moving = false;
	float m_changeRadius = 2.0f;
	CBSkybox* m_skyBoxInfo;
	Gameobject* m_sky;
	void Move(float delta);
	void GetNextTarget();
};