#include "FollowCamera.h"
#include "Input.h"
#include "Gameobject.h"

using namespace DirectX;

FollowCamera::FollowCamera() : Camera()
{
	m_theta = XM_PIDIV2;
	m_phi = 0.0001f;
}

void FollowCamera::Update(float delta)
{
	CheckFollowTarget();
		
	if (!b_followTarget)
	{
		DoFreeRoamCamera();
	}
	else
	{
		TrackAndFollowTarget();
	}
	
	Camera::Update(delta);
}

void FollowCamera::DoFreeRoamCamera()
{
	XMVECTOR position = XMLoadFloat3(&m_position);

	float mouseX = m_input->GetMouseMovementX();
	float mouseY = m_input->GetMouseMovementY();

	m_rotation.y += mouseX * 0.005f;
	m_rotation.x += mouseY * 0.005f;

	// Clamp the rotation to be within the range of -XM_PIDIV2 -> XM_PIDIV2
	// When we clamp we don't set the value to be exactly XM_PIDIV2, we add in a slight offset to prevent weird snapping behaviour.
	m_rotation.x = m_rotation.x < -XM_PIDIV2 ? -XM_PIDIV2 + 0.001f : m_rotation.x > XM_PIDIV2 ? XM_PIDIV2 - 0.001f : m_rotation.x;

	XMVECTOR forward = GetLookDirection();
	XMVECTOR up = XMLoadFloat3(&m_upDirection);

	XMVECTOR right = XMVector3Cross(up, forward);
	
	float movementForward = 0.0f;
	float movementRight = 0.0f;

	if (m_input->GetKeyState(DIKEYBOARD_W))
	{
		movementForward = m_movementSpeed;
	}
	else if (m_input->GetKeyState(DIKEYBOARD_S))
	{
		movementForward = -m_movementSpeed;
	}

	if (m_input->GetKeyState(DIKEYBOARD_A))
	{
		movementRight = -m_movementSpeed;
	}
	else if (m_input->GetKeyState(DIKEYBOARD_D))
	{
		movementRight = m_movementSpeed;
	}

	position += right * movementRight;
	position += forward * movementForward;

	XMStoreFloat3(&m_position, position);
}

void FollowCamera::TrackAndFollowTarget()
{
	m_theta -= m_input->GetMouseMovementY() * 0.01f;
	m_phi -= m_input->GetMouseMovementX() * 0.01f;

	if (m_theta > XM_PI)
	{
		m_theta = XM_PI - 0.001f;
	}
	else if (m_theta < 0.0f)
	{
		m_theta = 0.001f;
	}

	const float x = m_radius * sin(m_theta) * cos(m_phi);
	const float y = m_radius * sin(m_theta) * sin(m_phi);
	const float z = m_radius * cos(m_theta);
	
	XMFLOAT3 offset = XMFLOAT3(x, z, y);

	m_position.x = m_target->GetPosition().x + offset.x;
	m_position.y = m_target->GetPosition().y + offset.y;
	m_position.z = m_target->GetPosition().z + offset.z;

	LookAt(m_target->GetPosition());
}

void FollowCamera::SetInput(Input* input)
{
	m_input = input;
}

void FollowCamera::SetRotationSpeedScale(float speed)
{
	m_rotateSpeedScale = speed;
}

void FollowCamera::SetTarget(Gameobject* object)
{
	m_target = object;

	//FaceTarget();
}

void FollowCamera::FaceTarget()
{	
	Camera::LookAt(m_target->GetPosition());
}

void FollowCamera::SetMovementSpeed(float speed)
{
	m_movementSpeed = speed;
}

void FollowCamera::CheckFollowTarget()
{
	static bool previousState = false;

	if (m_input->GetKeyState(DIKEYBOARD_Q) && previousState == false)
	{
		previousState = true;
		b_followTarget = !b_followTarget;

		if (!b_followTarget)
		{
			FaceTarget();
		}
	}
	else if (!m_input->GetKeyState(DIKEYBOARD_Q) && previousState == true)
	{
		previousState = false;
	}
}