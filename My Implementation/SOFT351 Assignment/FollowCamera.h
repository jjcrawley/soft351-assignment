#pragma once
#include "Camera.h"

class Input;
class Gameobject;

class FollowCamera :
	public Camera
{
public:
	FollowCamera();
	virtual void Update(float delta) override;	
	void SetInput(Input* input);
	void SetRotationSpeedScale(float speed);
	void SetTarget(Gameobject* object);
	void FaceTarget();
	void SetMovementSpeed(float speed);	
private:
	bool b_followTarget = true;
	Gameobject* m_target;
	DirectX::XMFLOAT3 m_offset = DirectX::XMFLOAT3(0, 0, 0);
	DirectX::XMFLOAT3 m_targetOffset = DirectX::XMFLOAT3(0, 0, 0);
	Input* m_input;
	float m_theta = 0;
	float m_phi = 0;
	float m_radius = 50;
	float m_rotateSpeedScale = 0.01;
	float m_movementSpeed = 0.01;	
	void CheckFollowTarget();
	void TrackAndFollowTarget();
	void DoFreeRoamCamera();
};