#include "Gameobject.h"

using namespace DirectX;

Gameobject::Gameobject()
{
	m_position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_rotation = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_scale = XMFLOAT3(1.0f, 1.0f, 1.0f);

	m_matWorld = m_matPosition = m_matRotation = m_matScale = XMMatrixIdentity();

	SetUpWorldMatrix();
}

void Gameobject::CreateObject(Renderer* renderer)
{
	m_renderer = renderer;
}

void Gameobject::Render(ID3D11DeviceContext* context, const XMMATRIX& matProjection, const XMMATRIX& matView)
{
	XMMATRIX world = m_matWorld;	

	XMMATRIX mvp = world * matView * matProjection;
	
	m_renderer->Render(context, mvp, world);
}

void Gameobject::SetPosition(float x, float y, float z)
{
	m_position = XMFLOAT3(x, y, z);
}

void Gameobject::SetRotation(float x, float y, float z)
{
	m_rotation = XMFLOAT3(x, y, z);
}

void Gameobject::SetScale(float x, float y, float z)
{
	m_scale = XMFLOAT3(x,y,z);
}

XMFLOAT3 Gameobject::GetRotation()
{
	return m_rotation;
}

XMFLOAT3 Gameobject::GetPosition()
{
	return m_position;
}

XMFLOAT3 Gameobject::GetScale()
{
	return m_scale;
}

XMFLOAT3 Gameobject::GetWorldPosition()
{
	XMFLOAT3 worldPosition = m_position;

	if (m_parent != nullptr)
	{
		XMMATRIX parentWorld = m_parent->GetWorldMatrix();

		XMVECTOR posVec = XMLoadFloat3(&m_position);
		posVec = XMVector3Transform(posVec, parentWorld);

		XMStoreFloat3(&worldPosition, posVec);
	}

	return worldPosition;
}

BoundingBox Gameobject::GetBoundingBox()
{
	BoundingBox boundingBox = m_renderer->GetRenderingBounds();

	XMFLOAT3 worldPos = GetWorldPosition();
	
	XMFLOAT3 scaledMax;
	XMFLOAT3 scaledMin;

	scaledMax.x = boundingBox.maxPosition.x * m_scale.x + worldPos.x;
	scaledMax.y = boundingBox.maxPosition.y * m_scale.y + worldPos.y;
	scaledMax.z = boundingBox.maxPosition.z * m_scale.z + worldPos.z;

	scaledMin.x = boundingBox.minPosition.x * m_scale.x + worldPos.x;
	scaledMin.y = boundingBox.minPosition.y * m_scale.y + worldPos.y;
	scaledMin.z = boundingBox.minPosition.z * m_scale.z + worldPos.z;

	boundingBox.maxPosition = scaledMax;
	boundingBox.minPosition = scaledMin;

	return boundingBox;
}

XMMATRIX Gameobject::GetWorldMatrix()
{
	SetUpWorldMatrix();
	return m_matWorld;
}

void Gameobject::SetParent(Gameobject* object)
{
	if (object != this)
	{
		m_parent = object;
	}
}

DirectX::XMVECTOR Gameobject::GetDirection()
{
	const float x = cosf(m_rotation.x) * sinf(m_rotation.y);
	const float y = -sinf(m_rotation.x);
	const float z = cosf(m_rotation.x) * cosf(m_rotation.y);

	return XMVectorSet(x, y, z, 0.0f);
}

void Gameobject::SetUpWorldMatrix()
{
	XMMATRIX position = XMMatrixTranslation(m_position.x, m_position.y, m_position.z);
	XMMATRIX rotation = XMMatrixRotationRollPitchYaw(m_rotation.x, m_rotation.y, m_rotation.z);
	XMMATRIX scale = XMMatrixScaling(m_scale.x, m_scale.y, m_scale.z);
	
	XMMATRIX matWorld = scale * rotation * position;

	if (m_parent)
	{
		XMMATRIX worldMatrix = m_parent->GetWorldMatrix();

		matWorld *= worldMatrix;
	}

	m_matPosition = position;
	m_matRotation = rotation;
	m_matScale = scale;
	m_matWorld = matWorld;
}