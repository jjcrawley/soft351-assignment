#pragma once
#include <d3d11.h>
#include <dxgi.h>
#include <DirectXMath.h>
#include "Mesh.h"
#include "Renderer.h"

#ifndef GAMEOBJECT_BASE
#define GAMEOBJECT_BASE

class Gameobject
{
public:
	Gameobject();
	virtual void CreateObject(Renderer* renderer);
	virtual void Update(float delta) = 0;	
	virtual void Render(ID3D11DeviceContext* context, const DirectX::XMMATRIX& matProjection, const DirectX::XMMATRIX& matView);
	void SetPosition(float x, float y, float z);	
	void SetRotation(float x, float y, float z);
	void SetScale(float x, float y, float z);
	DirectX::XMFLOAT3 GetRotation();
	DirectX::XMFLOAT3 GetPosition();
	DirectX::XMFLOAT3 GetScale();
	DirectX::XMFLOAT3 GetWorldPosition();
	BoundingBox GetBoundingBox();
	DirectX::XMMATRIX GetWorldMatrix();
	void SetParent(Gameobject* object);
	DirectX::XMVECTOR GetDirection();
protected:
	DirectX::XMFLOAT3 m_position;
	DirectX::XMFLOAT3 m_rotation;
	DirectX::XMFLOAT3 m_scale;
	Renderer* m_renderer;	
	Gameobject* m_parent = nullptr;
	DirectX::XMMATRIX m_matWorld;
	DirectX::XMMATRIX m_matRotation;
	DirectX::XMMATRIX m_matPosition;
	DirectX::XMMATRIX m_matScale;
	void SetUpWorldMatrix();	
};
#endif // !GAMEOBJECT_BASE