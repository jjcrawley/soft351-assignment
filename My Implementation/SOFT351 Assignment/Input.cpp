#include "Input.h"
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")

void Input::InitialiseInput(HINSTANCE instance, HWND hwnd)
{	
	DirectInput8Create(instance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_directInput, nullptr);

	m_directInput->CreateDevice(GUID_SysKeyboard, &m_pKeyboard, nullptr);
	m_directInput->CreateDevice(GUID_SysMouse, &m_pMouse, nullptr);

	m_pKeyboard->SetDataFormat(&c_dfDIKeyboard);
	m_pKeyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

	m_pMouse->SetDataFormat(&c_dfDIMouse);
	m_pMouse->SetCooperativeLevel(hwnd, DISCL_EXCLUSIVE | DISCL_NOWINKEY);
}

bool Input::GetKeyState(BYTE key)
{		
	return (m_keyboardState[key] & 0x80) != 0;
}

bool Input::KeyPressed(BYTE key)
{
	return m_keyboardState[key] & 0x80 && !(m_previousKeyboardState[key] & 0x80);
}

float Input::GetMouseWheel()
{
	return (float)m_currentMouseState.lZ;
}

float Input::GetMouseMovementY()
{
	return (float)m_currentMouseState.lY;
}

float Input::GetMouseMovementX()
{
	return (float)m_currentMouseState.lX;
}

void Input::Update()
{
	m_pKeyboard->Acquire();
	m_pMouse->Acquire();

	m_pMouse->GetDeviceState(sizeof(DIMOUSESTATE), &m_currentMouseState);
	
	for (int i = 0; i < 256; i++)
	{
		m_previousKeyboardState[i] = m_keyboardState[i];
	}

	m_pKeyboard->GetDeviceState(sizeof(m_keyboardState), (LPVOID)&m_keyboardState);
}

void Input::Release()
{
	if (m_pKeyboard)
	{
		m_pKeyboard->Unacquire();
		m_pKeyboard->Release();
		m_pKeyboard = nullptr;
	}

	if (m_pMouse)
	{
		m_pMouse->Unacquire();
		m_pMouse->Release();
		m_pMouse = nullptr;
	}

	if (m_directInput)
	{
		m_directInput->Release();
		m_directInput = nullptr;
	}
}