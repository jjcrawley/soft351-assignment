#pragma once

#define DIRECTINPUT_VERSION DIRECTINPUT_HEADER_VERSION
#include <dinput.h>

#ifndef INPUT
#define INPUT
class Input
{
public:
	void InitialiseInput(HINSTANCE instance, HWND hwnd);	
	bool GetKeyState(BYTE key);
	bool KeyPressed(BYTE key);

	bool GetKeyState(unsigned int key)
	{
		return GetKeyState((BYTE) key);
	}

	bool KeyPressed(unsigned int key)
	{
		return GetKeyState((BYTE) key);
	}

	float GetMouseWheel();
	float GetMouseMovementY();
	float GetMouseMovementX();
	void Update();
	void Release();
private:	
	IDirectInputDevice8* m_pKeyboard;
	IDirectInputDevice8* m_pMouse;
	DIMOUSESTATE m_lastMouseState;
	LPDIRECTINPUT8 m_directInput;
	DIMOUSESTATE m_currentMouseState;
	BYTE m_keyboardState[256];
	BYTE m_previousKeyboardState[256];
};
#endif // !INPUT