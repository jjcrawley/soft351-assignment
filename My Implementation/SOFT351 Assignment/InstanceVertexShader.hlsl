
//**************************************************************************//
// No longer used; it is here to prove a point which is that you can have	//
// many constant buffers.													//
//**************************************************************************//
cbuffer cbChangeOnResize : register(b0)
{
	matrix Projection;
};

//**************************************************************************//
// Matrices constant buffer.												//
//**************************************************************************//
cbuffer cbChangesEveryFrame : register(b1)
{
	matrix MatWorld;
	matrix MatWorldViewProjection;
};

cbuffer cbPerSystem : register(b2)
{
	matrix particles[500];
};

//**************************************************************************//
// Vertex shader input structure.	The semantics (the things after the		//
// colon) look a little weird.  The semantics are used (so Microsoft tell	//
// us) used by the compiler to link shader inputs and outputs. 				//
//																			//
// For this to work, you must ensure that the vertex structure you use in	//
// any program that uses this shader is the same as below, vertex position,	//
// normal vector and texture U, V, in that order!							//
//**************************************************************************//
struct VS_INPUT
{
	float4 Pos       : POSITION;
	float3 VecNormal : NORMAL;
	float2 Tex       : TEXCOORD;
	float3 Tangent   : TANGENT;
//	float3 InstancePos : INSTANCEPOS;
	uint InstanceID  : SV_InstanceID;
};

//**************************************************************************//
// Vertex shader output strucrute, which also becomes the pixel shader input//
// structure.	The two things must be identical.							//
// Just a position and a texture UV.										//
//																			//
// NOTE: Pos has a different samentic to the structure above; get it wrong	//
// and nothing works.  That's because the pixel shader is in a different	//
// stage in the rendering pipeline.											//
//**************************************************************************//
struct VS_OUTPUT
{
	float4 Pos		 : SV_POSITION;
	float4 WorldPos  : POSITION;
	float3 VecNormal : NORMAL;
	float3 Tangent   : TANGENT;
	float2 Tex		 : TEXCOORD;
};

//**************************************************************************//
// Vertex Shader.															//
//**************************************************************************//
VS_OUTPUT VS_Instance(VS_INPUT input)
{
	VS_OUTPUT output;
	//**********************************************************************//
	// Multiply every vertex vy the WVP matrix (we do it "properly here		//
	// unlike the cubes sample.												//
	//**********************************************************************//
	
	input.Pos = mul(input.Pos, particles[input.InstanceID]);

	//input.Pos += float4(input.InstancePos, 0.0f);
	
	output.Pos = mul(input.Pos, MatWorldViewProjection);

	output.WorldPos = mul(input.Pos, MatWorld);

	//**********************************************************************//
	// Whatever we do to the tiger, we must also do to its normal vector.	//
	//**********************************************************************//
	output.VecNormal = mul(input.VecNormal, (float3x3)MatWorld);
	output.VecNormal = normalize(output.VecNormal);

	output.Tangent = mul(input.Tangent, (float3x3)MatWorld);
	output.Tangent = normalize(output.Tangent);

	//**********************************************************************//
	// And finally, just copu the texture Us and Vs to the output structure	//
	//**********************************************************************//
	output.Tex = input.Tex;

	return output;
}