#include "Light.h"

typedef DirectX::XMFLOAT3 XMFLOAT3;
typedef DirectX::XMFLOAT4 XMFLOAT4;

void Light::SetRange(float range)
{
	m_range = range;
}

void Light::SetAttenuation(XMFLOAT3 attenuation)
{
	m_attenuation = attenuation;
}

void Light::SetDirection(XMFLOAT3 direction)
{
	m_direction = direction;
}

void Light::SetConeAngle(float angle)
{
	m_cone = angle;
}

void Light::SetPosition(XMFLOAT3 position)
{
	m_position = position;
}

void Light::SetDiffuse(XMFLOAT4 diffuse)
{
	m_diffuse = diffuse;
}

void Light::SetLightType(LightType type)
{
	m_type = type;
}

XMFLOAT4 Light::GetDiffuse()
{
	return m_diffuse;
}

XMFLOAT3 Light::GetPosition()
{
	return m_position;
}

XMFLOAT3 Light::GetDirection()
{
	return m_direction;
}

float Light::GetRange()
{
	return m_range;
}

float Light::GetConeAngle()
{
	return m_cone;
}

void Light::SetEnabled(bool enabled)
{
	m_enabled = enabled;
}

bool Light::GetEnabled()
{
	return m_enabled;
}

LightType Light::GetLightType()
{
	return m_type;
}

LightInfo Light::GetLightInfo()
{
	LightInfo info;

	info.attenuation = m_attenuation;
	info.cone = m_cone;
	info.direction = m_direction;
	info.position = m_position;
	info.range = m_range;
	info.diffuse = m_diffuse;
	info.lightType = static_cast<int>(m_type);
	info.enabled = m_enabled;

	return info;
}