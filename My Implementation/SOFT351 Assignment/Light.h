#pragma once
#include <d3d11.h>
#include <DirectXMath.h>

struct LightInfo
{
	LightInfo()
	{
		ZeroMemory(this, sizeof(LightInfo));
	}
	DirectX::XMFLOAT3 direction;
	float cone;	
	DirectX::XMFLOAT4 diffuse;
	DirectX::XMFLOAT3 position;
	float range;
	DirectX::XMFLOAT3 attenuation;
	int lightType;	
	BOOL enabled = false;
	DirectX::XMFLOAT3 padding;
};

enum LightType
{
	Directional = 0,
	Point = 1,
	SpotLight = 2
};

#ifndef LIGHT
#define LIGHT

class Light
{
public:
	void SetRange(float range);
	void SetAttenuation(DirectX::XMFLOAT3 attenuation);
	void SetDirection(DirectX::XMFLOAT3 direction);
	void SetConeAngle(float angle);
	void SetPosition(DirectX::XMFLOAT3 position);
	void SetDiffuse(DirectX::XMFLOAT4 diffuse);
	void SetLightType(LightType type);
	DirectX::XMFLOAT4 GetDiffuse();
	DirectX::XMFLOAT3 GetPosition();
	DirectX::XMFLOAT3 GetDirection();
	float GetRange();
	float GetConeAngle();
	void SetEnabled(bool enabled);
	bool GetEnabled();
	LightType GetLightType();
	LightInfo GetLightInfo();
private:
	DirectX::XMFLOAT3 m_direction;
	float m_cone;	
	DirectX::XMFLOAT4 m_diffuse;
	DirectX::XMFLOAT3 m_position;
	float m_range;
	DirectX::XMFLOAT3 m_attenuation;
	LightType m_type;
	bool m_enabled;
};

#endif // !LIGHT