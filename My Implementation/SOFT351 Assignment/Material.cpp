#include "Material.h"

using namespace DirectX;

void Material::SetName(std::wstring name)
{
	m_name = std::wstring(name);
}

std::wstring Material::GetName()
{
	return m_name;
}

void Material::SetDiffuse(XMFLOAT4 diffuse)
{
	m_diffuse = diffuse;
}

void Material::SetAmbient(XMFLOAT4 ambient)
{
	m_ambient = ambient;
}

void Material::SetSpecular(XMFLOAT4 specular)
{
	m_specular = specular;
}

void Material::SetSpecularPower(float power)
{
	m_specularPower = power;
}

XMFLOAT4 Material::GetDiffuse()
{
	return m_diffuse;
}

XMFLOAT4 Material::GetAmbient()
{
	return m_ambient;
}

XMFLOAT4 Material::GetSpecular()
{
	return m_specular;
}

float Material::GetSpecularPower()
{
	return m_specularPower;
}

void Material::SetTextureName(std::wstring textureName)
{
	m_textureName = textureName;
}

std::wstring Material::GetTextureName()
{
	return m_textureName;
}

void Material::SetTextureIndex(int index, bool hasTexture)
{
	m_texIndex = index;
	b_hasTexture = hasTexture;
}

void Material::SetBumpMapIndex(int index, bool hasBumpMap)
{
	m_bumpIndex = index;
	b_hasBumpMap = hasBumpMap;
}

bool Material::HasTexture()
{
	return b_hasTexture;
}

bool Material::HasBumpMap()
{
	return b_hasBumpMap;
}

int Material::TextureIndex()
{
	return m_texIndex;
}

int Material::BumpMapIndex()
{
	return m_bumpIndex;
}
