#pragma once
#include <d3d11.h>
//#include <dxgi.h>
//#include <d3dx11.h>
#include <DirectXMath.h>
#include <vector>

#ifndef MATERIAL
#define MATERIAL
class Material
{
public:
	void SetName(std::wstring name);
	std::wstring GetName();
	void SetDiffuse(DirectX::XMFLOAT4 diffuse);
	void SetAmbient(DirectX::XMFLOAT4 ambient);
	void SetSpecular(DirectX::XMFLOAT4 specular);
	void SetSpecularPower(float power);
	DirectX::XMFLOAT4 GetDiffuse();
	DirectX::XMFLOAT4 GetAmbient();
	DirectX::XMFLOAT4 GetSpecular();
	float GetSpecularPower();
	void SetTextureName(std::wstring textureName);
	std::wstring GetTextureName();
	void SetTextureIndex(int index, bool hasTexture);
	void SetBumpMapIndex(int index, bool hasBumpMap);
	bool HasTexture();
	bool HasBumpMap();
	int TextureIndex();
	int BumpMapIndex();
private:
	std::wstring m_name = L"Empty";
	DirectX::XMFLOAT4 m_diffuse = DirectX::XMFLOAT4(0,0,0,0);
	DirectX::XMFLOAT4 m_ambient = DirectX::XMFLOAT4(0,0,0,0);
	DirectX::XMFLOAT4 m_specular = DirectX::XMFLOAT4(0,0,0,0);
	float m_specularPower = 0;
	std::wstring m_textureName;
	std::wstring m_bumpName;
	int m_texIndex = 0;
	int m_bumpIndex = 0;
	bool b_hasTexture = false;
	bool b_hasBumpMap = false;
};
#endif // !MATERIAL