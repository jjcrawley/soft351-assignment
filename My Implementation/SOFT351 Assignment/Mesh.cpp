#include "Mesh.h"

USHORT Mesh::GetIndexCount()
{
	return m_numIndices;
}

USHORT Mesh::GetVertexCount()
{
	return m_numVertices;
}

SimpleVertex* Mesh::GetVertices()
{
	return m_pVertices;
}

USHORT* Mesh::GetIndices()
{
	return m_pIndexes;
}

int Mesh::GetSubSetCount()
{
	return m_subSetCount;
}

std::wstring Mesh::GetPath()
{
	return m_meshPath;
}

void Mesh::SetPath(std::wstring path)
{
	m_meshPath = path;
}

std::vector<int>* Mesh::GetSubSetStart()
{
	return &m_subSetIndexStart;
}

std::vector<int>* Mesh::GetSubsetMaterialArray()
{
	return &m_subSetMaterialArray;
}

void Mesh::AssignVertices(std::vector<SimpleVertex>* vertices, USHORT numVertices)
{
	m_numVertices = USHORT(numVertices);
	m_pVertices = new SimpleVertex[numVertices];

	for (int i = 0; i < m_numVertices; i++)
	{
		SimpleVertex vertex = vertices->at(i);

		m_pVertices[i] = vertex;
	}
}

void Mesh::AssignIndices(std::vector<USHORT>* indices, USHORT numberIndices)
{
	m_numIndices = USHORT(numberIndices);
	m_pIndexes = new USHORT[m_numIndices];

	for (int i = 0; i < m_numIndices; i++)
	{
		m_pIndexes[i] = indices->at(i);
	}
}

void Mesh::AssignSubSetArray(std::vector<int>* subSetIndexArray, int subsetCount)
{
	m_subSetCount = subsetCount;

	for (int i = 0; i < subSetIndexArray->size(); i++)
	{
		m_subSetIndexStart.push_back(subSetIndexArray->at(i));
	}
}

void Mesh::AssignSubSetMaterials(std::vector<int>* subSetMaterialIndexes)
{
	for (int i = 0; i < subSetMaterialIndexes->size(); i++)
	{
		m_subSetMaterialArray.push_back(subSetMaterialIndexes->at(i));
	}
}

void Mesh::Release()
{
	delete[] m_pIndexes;
	delete[] m_pVertices;	
}