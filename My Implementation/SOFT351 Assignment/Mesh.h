#pragma once
#include <d3d11.h>
#include <dxgi.h>
//#include <d3dx11.h>
#include <DirectXMath.h>
#include <vector>

struct SimpleVertex
{
	SimpleVertex() {}
	
	SimpleVertex(float x, float y, float z, float u, float v, 
		float nx, float ny, float nz, float tx, float ty, float tz) :
	Pos(x,y,z), VecNormal(nx, ny, nz), TexUV(u, v), Tangent(tx, ty, tz){}
	
	DirectX::XMFLOAT3 Pos;	//Why not a float4?  See the shader strucrure.  Any thoughts?  Nigel
	DirectX::XMFLOAT3 VecNormal;
	DirectX::XMFLOAT2 TexUV;
	DirectX::XMFLOAT3 Tangent;
};

#ifndef MESH
#define MESH
class Mesh
{
public:
	USHORT GetIndexCount();
	USHORT GetVertexCount();
	SimpleVertex* GetVertices();
	USHORT* GetIndices();
	int GetSubSetCount();
	std::wstring GetPath();
	void SetPath(std::wstring path);
	std::vector<int>* GetSubSetStart();
	std::vector<int>* GetSubsetMaterialArray();
	void AssignVertices(std::vector<SimpleVertex>* vertices, USHORT numVertices);
	void AssignIndices(std::vector<USHORT>* indices, USHORT numberIndices);
	void AssignSubSetArray(std::vector<int>* subSetIndexArray, int subsetCount);
	void AssignSubSetMaterials(std::vector<int>* subSetMaterialIndexes);
	void Release();
private:
	SimpleVertex* m_pVertices;
	USHORT*	m_pIndexes;
	std::vector<int> m_subSetIndexStart;
	std::vector<int> m_subSetMaterialArray;
	int m_subSetCount;
	USHORT       m_numVertices;
	USHORT       m_numIndices;
	std::wstring m_meshPath;
};
#endif // !MESH