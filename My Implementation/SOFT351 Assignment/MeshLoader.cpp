#include "MeshLoader.h"
//#include <D3DX11tex.h>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <functional>
#include <cctype>

#include <DirectXTex.h>

using namespace DirectX;

void MeshLoader::Initialise(ID3D11Device* device, std::vector<Material*>* materials, std::vector<Mesh*>* meshes, std::vector<SimpleTexture*>* textures)
{
	m_device = device;
	m_textures = textures;
	m_materials = materials;
	m_meshes = meshes;
}

//this method can be dual purpose, it can also calculate the normals if necessary
std::vector<XMFLOAT3> MeshLoader::GetTangents(std::vector<SimpleVertex>* vertices, std::vector<USHORT>* indices, int faceCount)
{
	std::vector<XMFLOAT3> tempTangents;
	std::vector<XMFLOAT3> tangents;

	XMFLOAT3 tempTangent = XMFLOAT3(0.0f, 0.0f, 0.0f);

	//tc is short for texture coord, U1 and U2 are one edge, same for V1 and V2
	float tcU1, tcV1, tcV2, tcU2;

	//these represent x, y and z of an edge vector
	float vecX, vecY, vecZ;

	XMVECTOR edge1 = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	XMVECTOR edge2 = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);

	for (int face = 0; face < faceCount; face++)
	{
		//0 = top of triangle, 1 = bottom left, 2 = bottom right
		//extract x,y and z portion of face edges, goes from 0->2
		vecX = vertices->at(indices->at(face * 3)).Pos.x - vertices->at(indices->at(face * 3 + 2)).Pos.x;
		vecY = vertices->at(indices->at(face * 3)).Pos.y - vertices->at(indices->at(face * 3 + 2)).Pos.y;
		vecZ = vertices->at(indices->at(face * 3)).Pos.z - vertices->at(indices->at(face * 3 + 2)).Pos.z;
		edge1 = XMVectorSet(vecX, vecY, vecZ, 0.0f);

		//same as above, except we go from 2->1
		vecX = vertices->at(indices->at(face * 3 + 2)).Pos.x - vertices->at(indices->at(face * 3 + 1)).Pos.x;
		vecY = vertices->at(indices->at(face * 3 + 2)).Pos.y - vertices->at(indices->at(face * 3 + 1)).Pos.y;
		vecZ = vertices->at(indices->at(face * 3 + 2)).Pos.z - vertices->at(indices->at(face * 3 + 1)).Pos.z;
		edge2 = XMVectorSet(vecX, vecY, vecZ, 0.0f);

		//similiar to above, except now we're retrieving an edge vector for the texture coordinates, 0->2
		tcU1 = vertices->at(indices->at(face * 3)).TexUV.x - vertices->at(indices->at(face * 3 + 2)).TexUV.x;
		tcV1 = vertices->at(indices->at(face * 3)).TexUV.y - vertices->at(indices->at(face * 3 + 2)).TexUV.y;

		//texture coord stuff again, going from 2->1
		tcU2 = vertices->at(indices->at(face * 3 + 2)).TexUV.x - vertices->at(indices->at(face * 3 + 1)).TexUV.x;
		tcV2 = vertices->at(indices->at(face * 3 + 2)).TexUV.y - vertices->at(indices->at(face * 3 + 1)).TexUV.y;

		//create the tangent for the point
		tempTangent.x = (tcV1 * XMVectorGetX(edge1) - tcV2 * XMVectorGetX(edge2)) * (1.0f / (tcU1 * tcV2 - tcU2 * tcV1));
		tempTangent.y = (tcV1 * XMVectorGetY(edge1) - tcV2 * XMVectorGetY(edge2)) * (1.0f / (tcU1 * tcV2 - tcU2 * tcV1));
		tempTangent.z = (tcV1 * XMVectorGetZ(edge1) - tcV2 * XMVectorGetZ(edge2)) * (1.0f / (tcU1 * tcV2 - tcU2 * tcV1));
		tempTangents.push_back(tempTangent);
	}	

	//calculate the tangent for each vertex by averaging
	//loop through all the vertices
	for (int vert = 0; vert < vertices->size(); vert++)
	{
		int facesUsing = 0;
		XMVECTOR tangentSum = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);

		//check for which triangles use this vertex
		for (int triangle = 0; triangle < faceCount; triangle++)
		{
			if (indices->at(triangle * 3) == triangle ||
				indices->at(triangle * 3 + 1) == triangle ||
				indices->at(triangle * 3 + 2) == triangle)
			{			
				//t for tangent
				float tx, ty, tz;

				//add current tangent to the current tangents sum
				tx = XMVectorGetX(tangentSum) + tempTangents[triangle].x;
				ty = XMVectorGetY(tangentSum) + tempTangents[triangle].y;
				tz = XMVectorGetZ(tangentSum) + tempTangents[triangle].z;

				tangentSum = XMVectorSet(tx, ty, tz, 0.0f);

				facesUsing++;
			}
		}

		//average out the tangent sum
		tangentSum /= (float)facesUsing;

		//normalise the current averaged tangent
		tangentSum = XMVector3Normalize(tangentSum);

		XMFLOAT3 tangent = XMFLOAT3(0.0f, 0.0f, 0.0f);

		tangent.x = XMVectorGetX(tangentSum);
		tangent.y = XMVectorGetY(tangentSum);
		tangent.z = XMVectorGetZ(tangentSum);

		tangents.push_back(tangent);
	}

	return tangents;
}

void MeshLoader::CharStrToWideChar(WCHAR* dest, char* source)
{
	int length = (int)strlen(source);
	for (int i = 0; i <= length; i++)
		dest[i] = (WCHAR)source[i];
}

std::wstring MeshLoader::TrimStart(std::wstring s)
{
	s.erase(s.begin(), std::find_if(s.begin(),
		s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

std::wstring MeshLoader::TrimEnd(std::wstring s)
{
	s.erase(std::find_if(s.rbegin(), s.rend(),
		std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

Mesh* MeshLoader::LoadMesh(LPSTR filename, bool LHS)
{
	WCHAR pathCheck[300];

	CharStrToWideChar(pathCheck, filename);

	std::wstring path(pathCheck);

	for (int i = 0; i < m_meshes->size(); i++)
	{
		if (path == m_meshes->at(i)->GetPath())
		{
			return m_meshes->at(i);
		}
	}

	std::wifstream          fileStream;
	std::wstring            line;

	std::vector <XMFLOAT3> vectorVertices(0);
	std::vector <XMFLOAT2> vertexTextures(0);
	std::vector <XMFLOAT3> vertexNormals(0);
	std::vector <XMFLOAT3> tangents(0);
	std::vector <SimpleVertex> vertexes(0);

	int vertexCount = 0;
	std::vector <USHORT> indices(0);
	int indexCount = 0;
	int faceCount = 0;

	std::vector <USHORT>    vectorIndices(0);
	std::vector <USHORT>	textureIndices(0);
	std::vector <USHORT>	normalIndices(0);

	std::vector<int> subSetStarts;
	int subSetCount = 0;
	std::vector<std::wstring> materialNames(0);

	fileStream.open(filename);
	bool isOpen = fileStream.is_open();		//debugging only.

	if (!isOpen)
	{
		return nullptr;
	}

	std::wstring			materialLibraryName;

	while (std::getline(fileStream, line))
	{
		line = TrimStart(line);

		if (line.compare(0, 1, L"#") == 0)
		{
			continue;
		}

		if (line.compare(0, 6, L"mtllib") == 0)
		{
			materialLibraryName = line.erase(0, 7);
		}
		else if (line.compare(0, 6, L"usemtl") == 0)
		{
			std::wstring matName;

			matName = line.erase(0, 7);

			materialNames.push_back(matName);

			if (materialNames.size() > subSetCount)
			{
				subSetStarts.push_back(indexCount);
				subSetCount++;
			}
		}
		else if (line.compare(0, 2, L"v ") == 0)  //"v space"
		{
			std::wstringstream stream(line);

			stream.ignore(2);
			
			float x, y, z;			

			stream >> x;
			stream >> y;
			stream >> z;

			XMFLOAT3 v;

			if (LHS)
			{
				v.x = x; v.y = y; v.z = z;
			}
			else
			{
				v.x = x; v.y = y; v.z = -z;
			}

			vectorVertices.push_back(v);
		}
		else if (line.compare(0, 3, L"vt ") == 0)
		{
			std::wstringstream stream(line);

			stream.ignore(3);			
			
			float x, y;			

			stream >> x;
			stream >> y;

			XMFLOAT2 textureCoord;
			textureCoord.x = x;

			if (LHS)
			{
				textureCoord.y = y;
			}
			else
			{
				textureCoord.y = 1 - y;
			}

			vertexTextures.push_back(textureCoord);
		}
		else if (line.compare(0, 3, L"vn ") == 0)
		{
			std::wstringstream stream(line);

			stream.ignore(2);
			
			float x, y, z;

			stream >> x;
			stream >> y;
			stream >> z;

			XMFLOAT3 vertexNormal;
			vertexNormal.x = x;
			vertexNormal.y = y;

			if (LHS)
			{
				vertexNormal.z = z;
			}
			else
			{
				vertexNormal.z = -z;
			}

			vertexNormals.push_back(vertexNormal);
		}
		else if (line.compare(0, 2, L"g ") == 0)
		{
			subSetStarts.push_back(indexCount);
			subSetCount++;
		}
		else if (line.compare(0, 2, L"f ") == 0)  //"f space"
		{			
			std::wstringstream stream(line);

			UINT v1, vt1, vn1, v2, vt2, vn2, v3, vt3, vn3;			

			stream.ignore(2);

			//the extra gets are to consume the / in the face definitions

			stream >> v1;
			stream.get();
			stream >> vt1;
			stream.get();
			stream >> vn1;
			stream.get();

			stream >> v2;
			stream.get();
			stream >> vt2;
			stream.get();
			stream >> vn2;
			stream.get();

			stream >> v3;
			stream.get();
			stream >> vt3;
			stream.get();
			stream >> vn3;
			stream.get();

			if (subSetCount == 0)
			{
				subSetStarts.push_back(indexCount);
				subSetCount++;
			}

			for (int currentVertex = 0; currentVertex < 3; currentVertex++)
			{
				UINT tempVertexIndex;
				UINT tempUVIndex;
				UINT tempNormIndex;

				if (currentVertex == 0)
				{
					tempUVIndex = vt1 - 1;
					tempVertexIndex = v1 - 1;
					tempNormIndex = vn1 - 1;
				}
				else if (currentVertex == 1)
				{
					tempUVIndex = vt2 - 1;
					tempVertexIndex = v2 - 1;
					tempNormIndex = vn2 - 1;
				}
				else
				{
					// current vertex has to be 2
					tempUVIndex = vt3 - 1;
					tempVertexIndex = v3 - 1;
					tempNormIndex = vn3 - 1;
				}

				bool exists = false;

				for (int iCheck = 0; iCheck < vertexCount; iCheck++)
				{
					if (vectorIndices[iCheck] == tempVertexIndex)
					{
						if (textureIndices[iCheck] == tempUVIndex)
						{
							if (normalIndices[iCheck] == tempNormIndex)
							{
								indices.push_back((USHORT)iCheck);
								exists = true;
								break;
							}
						}
					}
				}

				if (!exists)
				{
					vectorIndices.push_back((USHORT)tempVertexIndex);
					textureIndices.push_back((USHORT) tempUVIndex);
					normalIndices.push_back((USHORT) tempNormIndex);
					indices.push_back((USHORT) vertexCount);
					vertexCount++;
				}

				indexCount++;
			}

			faceCount++;
		}
	}

	fileStream.close();

	WCHAR materialFilePath[300];

	CharStrToWideChar(materialFilePath, filename);

	std::wstring materialFilename = materialFilePath;

	materialFilename.erase(materialFilename.find_last_of(L"\\"));

	materialFilename.append(L"\\");

	std::wstring folderPath = materialFilename;

	materialFilename.append(materialLibraryName);

	fileStream.open(materialFilename);

	int count = 0;
	Material* p_currentMaterial = nullptr;
	bool hasBumpMap = false;

	while (std::getline(fileStream, line))
	{
		line = TrimStart(line);
		if (line.compare(0, 6, L"newmtl") == 0)
		{
			std::wstring name = line.erase(0, 7);

			Material* p_material = new Material();
			p_material->SetName(name);

			p_currentMaterial = p_material;

			p_currentMaterial->SetTextureIndex(0, false);

			m_materials->push_back(p_material);

			count++;
		}
		else if (line.compare(0, 1, L"K") == 0)
		{
			if (line.compare(1, 1, L"d") == 0)
			{
				std::wstringstream stream(line);

				stream.ignore(3);

				XMFLOAT4 materialDiffuse;

				stream >> materialDiffuse.x;
				stream >> materialDiffuse.y;
				stream >> materialDiffuse.z;

				p_currentMaterial->SetDiffuse(materialDiffuse);
			}
			else if (line.compare(1, 1, L"s") == 0)
			{
				std::wstringstream stream(line);

				stream.ignore(3);

				XMFLOAT4 materialSpecular;

				stream >> materialSpecular.x;
				stream >> materialSpecular.y;
				stream >> materialSpecular.z;

				p_currentMaterial->SetSpecular(materialSpecular);
			}
			else if (line.compare(1, 1, L"a") == 0)
			{
				std::wstringstream stream(line);

				stream.ignore(3);

				XMFLOAT4 materialAmbient;

				stream >> materialAmbient.x;
				stream >> materialAmbient.y;
				stream >> materialAmbient.z;

				p_currentMaterial->SetAmbient(materialAmbient);
			}
		}
		else if (line.compare(0, 6, L"map_Kd") == 0)
		{
			std::wstring textureName = line.erase(0, 7);

			p_currentMaterial->SetTextureName(textureName);

			std::wstring texturePath = folderPath + textureName; //folderPath.append(textureName);

			bool loaded = false;

			for (int i = 0; i < m_textures->size(); i++)
			{
				if (textureName == m_textures->at(i)->GetName())
				{
					loaded = true;
					p_currentMaterial->SetTextureIndex(i, true);
				}
			}

			if (!loaded)
			{
				SimpleTexture* texture = LoadTexture(texturePath);

				if (texture)
				{
					p_currentMaterial->SetTextureIndex((int) m_textures->size() - 1, true);
				}

				//ID3D11ShaderResourceView* tempSRV;

				//HRESULT result = D3DX11CreateShaderResourceViewFromFile(m_device,
				//	texturePath.c_str(),
				//	NULL, NULL,
				//	&tempSRV,		// This is returned.
				//	NULL);

				//if (SUCCEEDED(result))
				//{
				//	SimpleTexture* texture = new SimpleTexture();

				//	texture->SetName(texturePath);
				//	texture->SetShaderResource(tempSRV);
				//	p_currentMaterial->SetTextureIndex((int)m_textures->size(), true);
				//	m_textures->push_back(texture);
				//}
			}			
		}
		else if (line.compare(0, 8, L"map_bump") == 0)
		{
			std::wstring bumpName = line.erase(0, 9);

			std::wstring bumpPath = folderPath + bumpName; //folderPath.append(bumpName);

			bool loaded = false;
			hasBumpMap = true;

			for (int i = 0; i < m_textures->size(); i++)
			{
				if (bumpName == m_textures->at(i)->GetName())
				{
					loaded = true;
					p_currentMaterial->SetBumpMapIndex(i, true);
				}
			}

			if (!loaded)
			{
				SimpleTexture* texture = LoadTexture(bumpPath);

				if (texture)
				{
					p_currentMaterial->SetBumpMapIndex((int) m_textures->size() - 1, true);
				}

				/*ID3D11ShaderResourceView* tempBumpR;

				HRESULT result = D3DX11CreateShaderResourceViewFromFile(m_device,
					bumpPath.c_str(),
					nullptr, nullptr,
					&tempBumpR,
					nullptr);

				if (SUCCEEDED(result))
				{
					SimpleTexture* texture = new SimpleTexture();
					texture->SetName(bumpPath);
					texture->SetShaderResource(tempBumpR);
					p_currentMaterial->SetBumpMapIndex((int)m_textures->size(), true);
					m_textures->push_back(texture);
				}*/
			}
		}
		else if (line.compare(0, 2, L"Ns") == 0)
		{
			std::wstringstream stream(line);

			stream.ignore(3);

			float power;

			stream >> power;

			p_currentMaterial->SetSpecularPower(power);
		}
	}

	subSetStarts.push_back(indexCount);

	if (subSetStarts[1] == 0)
	{
		subSetStarts.erase(subSetStarts.begin() + 1);
		subSetCount--;
	}

	Mesh* mesh = new Mesh();

	std::vector<int> subSetMaterialArray;

	mesh->AssignSubSetArray(&subSetStarts, subSetCount);

	for (int i = 0; i < subSetCount; i++)
	{
		bool hasMaterial = false;

		for (int j = 0; j < m_materials->size(); j++)
		{
			if (m_materials->at(j)->GetName() == materialNames[i])
			{
				subSetMaterialArray.push_back(j);
				hasMaterial = true;
			}
		}

		if (!hasMaterial)
		{
			subSetMaterialArray.push_back(0);
		}
	}

	mesh->AssignSubSetMaterials(&subSetMaterialArray);
	
	for (int i = 0; i < vertexCount; i++)
	{
		SimpleVertex vertex;

		vertex.Pos = vectorVertices[vectorIndices[i]];
		vertex.TexUV = vertexTextures[textureIndices[i]];
		vertex.VecNormal = vertexNormals[normalIndices[i]];

		vertexes.push_back(vertex);
	}

	//optimisation, checks if a bump map was loaded, if it was then calculate the tangents
	//probably unnecessary
	if (hasBumpMap)
	{
		tangents = GetTangents(&vertexes, &indices, faceCount);

		for (int i = 0; i < vertexCount; i++)
		{
			vertexes[i].Tangent = tangents[i];
		}
	}	

	mesh->AssignVertices(&vertexes, (USHORT)vertexCount);

	mesh->AssignIndices(&indices, (USHORT)indices.size());

	mesh->SetPath(pathCheck);

	return mesh;
}

SimpleTexture * MeshLoader::LoadTexture(const std::wstring& filePath)
{
	size_t lastDot = filePath.rfind('.');

	std::wstring extension = filePath.substr(lastDot);

	HRESULT result;

	ScratchImage* image = new ScratchImage;
	TexMetadata metaData;

	if (extension == L".dds")
	{		
		result = LoadFromDDSFile(filePath.c_str(), DDS_FLAGS::DDS_FLAGS_NONE, &metaData, *image);	
	}
	else if (extension == L".tga")
	{
		result = LoadFromTGAFile(filePath.c_str(), &metaData, *image);		
	}
	else
	{
		result = LoadFromWICFile(filePath.c_str(), WIC_FLAGS::WIC_FLAGS_NONE, &metaData, *image, nullptr);		
	}

	ID3D11ShaderResourceView* tempSRV = nullptr;

	if (SUCCEEDED(result))
	{
		/*ScratchImage mipChain;

		result = GenerateMipMaps(image->GetImages(), 1, metaData, TEX_FILTER_BOX, 0, mipChain);

		if (SUCCEEDED(result))
		{
			result = CreateShaderResourceView(m_device, mipChain.GetImages(), 1, metaData, &tempSRV);
		}

		mipChain.Release();*/

		if (SUCCEEDED(result))
		{
			result = CreateShaderResourceView(m_device, image->GetImages(), 1, metaData, &tempSRV);
		}
	}

	if (FAILED(result))
	{
		return nullptr;
	}
	
	SimpleTexture* texture = new SimpleTexture();

	texture->SetName(filePath);
	texture->SetShaderResource(tempSRV);
	m_textures->push_back(texture);

	delete image;

	return texture;
}