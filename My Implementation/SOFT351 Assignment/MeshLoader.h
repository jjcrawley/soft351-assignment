#pragma once
#include <d3d11.h>
#include <string>		
#include <vector>
#include "Mesh.h"
#include "SimpleTexture.h"
#include "Material.h"

#ifndef MESH_LOADER
#define MESHLOADER

class MeshLoader
{
public:
	void Initialise(ID3D11Device* device, std::vector<Material*>* materials, std::vector<Mesh*>* meshes, std::vector<SimpleTexture*>* textures);
	Mesh* LoadMesh(LPSTR fileName, bool LHS);
	SimpleTexture* LoadTexture(const std::wstring& filePath);
private:
	ID3D11Device* m_device;
	std::vector<Material*>* m_materials;
	std::vector<Mesh*>* m_meshes;
	std::vector<SimpleTexture*>* m_textures;
	std::vector<DirectX::XMFLOAT3> GetTangents(std::vector<SimpleVertex>* vertices, std::vector<USHORT>* indices, int faceCount);
	void CharStrToWideChar(WCHAR *dest, char *source);
	std::wstring TrimStart(std::wstring s);
	std::wstring TrimEnd(std::wstring s);
};

#endif