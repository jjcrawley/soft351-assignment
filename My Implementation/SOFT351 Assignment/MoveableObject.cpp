#include "MoveableObject.h"
#include "Input.h"

using namespace DirectX;

void MoveableObject::SetInputManager(Input* input)
{
	m_inputManager = input;
}

void MoveableObject::Update(float delta)
{
	SetUpWorldMatrix();

	CheckControl();

	if (!b_controllable)
	{
		return;
	}

	if (m_inputManager->GetKeyState(DIKEYBOARD_A))
	{
		m_rotation.y -= 1.0f * delta;
	}
	else if (m_inputManager->GetKeyState(DIKEYBOARD_D))
	{
		m_rotation.y += 1.0f * delta;
	}
	
	float speed = 0;

	if (m_inputManager->GetKeyState(DIKEYBOARD_W))
	{
		speed = m_speed;
	}
	else if (m_inputManager->GetKeyState(DIKEYBOARD_S))
	{
		speed = -m_speed;
	}

	MoveForward(speed * delta);
}

void MoveableObject::SetSpeed(float speed)
{
	m_speed = speed;
}

float MoveableObject::GetSpeed()
{
	return m_speed;
}

void MoveableObject::MoveForward(float speed)
{
	XMVECTOR position = XMLoadFloat3(&m_position);

	const float tempRotation = m_rotation.y;
	m_rotation.y -= XM_PIDIV2;

	XMVECTOR direction = GetDirection();

	m_rotation.y = tempRotation;

	position += direction * speed;

	XMStoreFloat3(&m_position, position);

	/*XMVECTOR initialDirection = GetDirection();
	XMVECTOR currentVectorDirection = XMLoadFloat3(&m_currentDirectionVec);

	XMMATRIX rotationMatrix = m_matRotation;

	currentVectorDirection = XMVector3TransformCoord(initialDirection, rotationMatrix);
	currentVectorDirection = XMVector3Normalize(currentVectorDirection);

	XMStoreFloat3(&m_currentDirectionVec, currentVectorDirection);
		
	currentVectorDirection *= speed;

	m_position.x += XMVectorGetX(currentVectorDirection);
	m_position.y += XMVectorGetY(currentVectorDirection);
	m_position.z += XMVectorGetZ(currentVectorDirection);	*/
}

void MoveableObject::CheckControl()
{
	static bool previousState = false;

	if (m_inputManager->GetKeyState(DIKEYBOARD_Q) && previousState == false)
	{
		previousState = true;
		b_controllable = !b_controllable;		
	}
	else if (!m_inputManager->GetKeyState(DIKEYBOARD_Q) && previousState == true)
	{
		previousState = false;
	}
}