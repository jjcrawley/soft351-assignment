#pragma once
#include "Gameobject.h"

#ifndef MOVEABLEOBJECT
#define MOVEABLEOBJECT

class Input;

class MoveableObject :
	public Gameobject
{
public:
	void SetInputManager(Input* input);
	virtual void Update(float delta) override;
	void SetSpeed(float speed);
	float GetSpeed();
private:
	Input* m_inputManager;
	float m_speed = 5.0f;	
	bool b_controllable = true;
	void CheckControl();
protected:
	DirectX::XMFLOAT3 m_currentDirectionVec;
	void MoveForward(float speed);
};

#endif // !1