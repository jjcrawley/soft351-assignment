#include "Renderer.h"

using namespace DirectX;

HRESULT Renderer::Initialise(ID3D11Device* device, Mesh* mesh, ID3D11Buffer* changesEveryFrame, ID3D11Buffer* materialChanges)
{
	m_mesh = mesh;

	HRESULT result = S_OK;

	D3D11_BUFFER_DESC indexBufferDesc;
	ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(USHORT) * m_mesh->GetIndexCount();
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA initData;

	initData.pSysMem = &m_mesh->GetIndices()[0];
	device->CreateBuffer(&indexBufferDesc, &initData, &m_indexBuffer);

#ifdef _DEBUG
	{
		const char name[] = "Index buffer";
		m_indexBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	D3D11_BUFFER_DESC vertexBufferDesc;
	ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(SimpleVertex) * m_mesh->GetVertexCount();
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;

	ZeroMemory(&vertexBufferData, sizeof(vertexBufferData));
	vertexBufferData.pSysMem = &m_mesh->GetVertices()[0];
	
	result = device->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &m_vertexBuffer);

#ifdef _DEBUG
	{
		const char name[] = "Vertex buffer";
		m_vertexBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	//m_context = context;

	m_pChangeEveryFrame = changesEveryFrame;
	m_pMaterialChanges = materialChanges;

	m_boundingBox = GetBoundingBox();

	/*std::string aString;
	aString = std::to_string(m_boundingBox.maxPosition.x) + " " + std::to_string(m_boundingBox.maxPosition.y) + " " + std::to_string(m_boundingBox.maxPosition.z) + ", ";
	aString = aString + std::to_string(m_boundingBox.minPosition.x) + " " + std::to_string(m_boundingBox.minPosition.y) + " " + std::to_string(m_boundingBox.minPosition.z) + "\n";

	OutputDebugStringA(aString.c_str());*/	

	return result;
}

void Renderer::Render(ID3D11DeviceContext* context, const XMMATRIX& mvp, const XMMATRIX& world)
{
	context->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R16_UINT, 0);

	UINT stride = sizeof(SimpleVertex);
	UINT offset = 0;
	context->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	CBChangesEveryFrame cb;	
	cb.matWorld = XMMatrixTranspose(world);
	cb.matWorldViewProjection = XMMatrixTranspose(mvp);	
	
	context->UpdateSubresource(m_pChangeEveryFrame, 0, NULL, &cb, 0, 0);

	CBMaterial cbColour;

	for (int j = 0; j < m_mesh->GetSubSetCount(); ++j)
	{		
		int matIndex = m_mesh->GetSubsetMaterialArray()->at(j);

		cbColour.hasTexture = m_materials->at(matIndex)->HasTexture();
		cbColour.hasBumpMap = m_materials->at(matIndex)->HasBumpMap();
		cbColour.diffuse = m_materials->at(matIndex)->GetDiffuse();
		cbColour.ambient = m_materials->at(matIndex)->GetAmbient();
		cbColour.specular = m_materials->at(matIndex)->GetSpecular();
		cbColour.specularPower = m_materials->at(matIndex)->GetSpecularPower();

		context->UpdateSubresource(m_pMaterialChanges, 0, NULL, &cbColour, 0, 0);
				
		context->PSSetConstantBuffers(0, 1, &m_pMaterialChanges);		//Note this one belongs to the pixel shader.		
		context->VSSetConstantBuffers(1, 1, &m_pChangeEveryFrame);	// constant buffers.

		context->PSSetConstantBuffers(2, 1, &m_pChangeEveryFrame);		

		if (m_materials->at(matIndex)->HasTexture())
		{
			int textureIndex = m_materials->at(matIndex)->TextureIndex();

			ID3D11ShaderResourceView* view = m_textures->at(textureIndex)->GetView();

			context->PSSetShaderResources(0, 1, &view);
		}

		if (m_materials->at(matIndex)->HasBumpMap())
		{
			int bumpIndex = m_materials->at(matIndex)->BumpMapIndex();

			ID3D11ShaderResourceView* view = m_textures->at(bumpIndex)->GetView();

			context->PSSetShaderResources(1, 1, &view);
		}

		int indexStart = m_mesh->GetSubSetStart()->at(j);
		int drawAmount = m_mesh->GetSubSetStart()->at(j + 1) - m_mesh->GetSubSetStart()->at(j);

		context->DrawIndexed(drawAmount, indexStart, 0);
	}
}

void Renderer::SetTextureResources(std::vector<SimpleTexture*>* textureResources)
{
	m_textures = textureResources;
}

void Renderer::SetMaterialResources(std::vector<Material*>* materials)
{
	m_materials = materials;
}

BoundingBox Renderer::GetRenderingBounds()
{
	return m_boundingBox;
}

void Renderer::Release()
{
	if (m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = nullptr;
	}

	if (m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = nullptr;
	}

	m_mesh = nullptr;
	m_pChangeEveryFrame = nullptr;
	m_pMaterialChanges = nullptr;
	m_textures = nullptr;
	m_materials = nullptr;
}

BoundingBox Renderer::GetBoundingBox()
{
	BoundingBox bounds;

	SimpleVertex* vertexes = m_mesh->GetVertices();

	XMFLOAT3 maxPosition = XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	XMFLOAT3 minPosition = XMFLOAT3(FLT_MAX, FLT_MAX, FLT_MAX);

	for (int i = 0; i < m_mesh->GetVertexCount(); i++)
	{
		XMFLOAT3 position = vertexes[i].Pos;

		maxPosition.x = max(maxPosition.x, position.x);
		maxPosition.y = max(maxPosition.y, position.y);
		maxPosition.z = max(maxPosition.z, position.z);

		minPosition.x = min(minPosition.x, position.x);
		minPosition.y = min(minPosition.y, position.y);
		minPosition.z = min(minPosition.z, position.z);
	}

	bounds.maxPosition = maxPosition;
	bounds.minPosition = minPosition;

	return bounds;
}