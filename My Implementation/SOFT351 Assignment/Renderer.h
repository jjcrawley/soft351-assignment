#pragma once
#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>		
#include <string>
#include "Mesh.h"
#include "Material.h"
#include "SimpleTexture.h"

struct CBMaterial
{
	DirectX::XMFLOAT4 diffuse;
	DirectX::XMFLOAT4 ambient;
	DirectX::XMFLOAT4 specular;
	float specularPower;
	BOOL hasTexture;
	BOOL hasBumpMap;
	float padding;
};

struct CBSkybox
{
	DirectX::XMFLOAT4 colour;
	float time;
	DirectX::XMFLOAT3 crawlerPos;
};

//**************************************************************************//
// Light vector never moves; and colour never changes.  I have done it .	//
// this way to show how constant buffers can be used so that you don't		//
// upsate stuff you don't need to.											//
// Beware of constant buffers that aren't in multiples of 16 bytes..		//
//**************************************************************************//
struct CBChangeOnResize
{
	DirectX::XMMATRIX matProjection;
};

//**************************************************************************//
// Note we do it properly here and pass the WVP matrix, rather than world,	//
// view and projection matrices separately.									//
//																			//
// We still need the world matrix to transform the normal vectors.			//
//**************************************************************************//
struct CBChangesEveryFrame
{
	DirectX::XMMATRIX matWorld;
	DirectX::XMMATRIX matWorldViewProjection;
};

struct BoundingBox
{
	DirectX::XMFLOAT3 minPosition;
	DirectX::XMFLOAT3 maxPosition;
};

struct BoundingSphere
{
	DirectX::XMFLOAT3 centre;
	float radius;
};

#ifndef RENDERER
#define RENDERER
class Renderer
{
public:
	HRESULT Initialise(ID3D11Device* device, Mesh* mesh, ID3D11Buffer* changesEveryFrame, ID3D11Buffer* neverChanges);
	void Render(ID3D11DeviceContext* context, const DirectX::XMMATRIX &mvp, const DirectX::XMMATRIX &world);
	void SetTextureResources(std::vector<SimpleTexture*>* textureResources);
	void SetMaterialResources(std::vector<Material*>* materials);
	BoundingBox GetRenderingBounds();
	void Release();
private:
	Mesh* m_mesh;
	ID3D11Buffer* m_indexBuffer;
	ID3D11Buffer* m_vertexBuffer;
	ID3D11Buffer* m_pChangeEveryFrame;
	ID3D11Buffer* m_pMaterialChanges;
	std::vector<SimpleTexture*>* m_textures;
	std::vector<Material*>* m_materials;
	BoundingBox m_boundingBox;
	BoundingBox GetBoundingBox();
};
#endif // !1