#include "RotatingObject.h"

void RotatingObject::Update(float delta)
{
	m_rotation.y += delta * m_rotatingSpeed;

	SetUpWorldMatrix();	
}