#pragma once
#include "Gameobject.h"

#ifndef ROTATINGOBJECT
#define ROTATINGOBJECT

class RotatingObject :
	public Gameobject
{
public:
	virtual void Update(float time) override;
private:
	float m_rotatingSpeed = 1.0f;
};
#endif // !ROTATINGOBJECT