#pragma once
#include <d3d11.h>

class ShaderHelper
{
public:
	static HRESULT CompileShaderFromFile(WCHAR* szFileName,		// File Name
		LPCSTR szEntryPoint,		// Namee of shader
		LPCSTR szShaderModel,		// Shader model
		ID3DBlob** ppBlobOut);		// Blob returned
private:
	static void charStrToWideChar(WCHAR *dest, char *source);
};