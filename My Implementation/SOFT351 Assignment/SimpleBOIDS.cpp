#include "SimpleBOIDS.h"

using namespace DirectX;

void SimpleBOIDS::Update(float delta)
{
	for (int i = 0; i < NUM_BOIDS; i++)
	{
		m_boids[i]->Update();
		m_boids[i]->Move(delta);
		m_boids[i]->ClampPosition(m_boundingRect);
	}

	//for (int i = 0; i < NUM_BOIDS; i++)
	//{
	//	m_boids[i]->Move(delta);
	//	m_boids[i]->ClampPosition(m_boundingRect);
	//}
}

void SimpleBOIDS::Render(ID3D11DeviceContext* context)
{
	UINT strides = sizeof(SimpleVertex);
	UINT offsets = 0;

	ID3D11Buffer* instanceBuffer = m_boidsVertexBuffer;

	context->IASetInputLayout(m_boidsLayout);

	context->IASetIndexBuffer(m_boidsIndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	context->IASetVertexBuffers(0, 1, &instanceBuffer, &strides, &offsets);

	context->PSSetShader(m_pixelShader, NULL, 0);
	context->VSSetShader(m_vertexShader, NULL, 0);

	XMMATRIX matWorldPos = XMMatrixTranslation(0.0f, 0.0f, 0.0f);

	XMMATRIX matWVP = matWorldPos * m_camera->GetViewMatrix() * m_camera->GetProjectionMatrix();

	CBChangesEveryFrame buffer;
	buffer.matWorld = XMMatrixTranspose(matWorldPos);
	buffer.matWorldViewProjection = XMMatrixTranspose(matWVP);
	
	context->UpdateSubresource(m_perObjectBuffer, 0, NULL, &buffer, 0, 0);

	UpdateBOIDBuffer(context);

	context->RSSetState(m_rasterizerState);	
	context->PSSetSamplers(0, 1, &m_sampler);

	CBMaterial cbColour;

	for (int j = 0; j < m_boidsMesh->GetSubSetCount(); j++)
	{
		int matIndex = m_boidsMesh->GetSubsetMaterialArray()->at(j);

		cbColour.hasTexture = m_materials->at(matIndex)->HasTexture();
		cbColour.hasBumpMap = m_materials->at(matIndex)->HasBumpMap();
		cbColour.diffuse = m_materials->at(matIndex)->GetDiffuse();
		cbColour.ambient = m_materials->at(matIndex)->GetAmbient();
		cbColour.specular = m_materials->at(matIndex)->GetSpecular();
		cbColour.specularPower = m_materials->at(matIndex)->GetSpecularPower();

		context->UpdateSubresource(m_materialBuffer, 0, NULL, &cbColour, 0, 0);

		context->PSSetConstantBuffers(0, 1, &m_materialBuffer);		//Note this one belongs to the pixel shader.		
		context->VSSetConstantBuffers(1, 1, &m_perObjectBuffer);	// constant buffers.
		context->VSSetConstantBuffers(2, 1, &m_instanceBuffer);

		context->PSSetConstantBuffers(2, 1, &m_perObjectBuffer);

		if (m_materials->at(matIndex)->HasTexture())
		{
			int textureIndex = m_materials->at(matIndex)->TextureIndex();

			ID3D11ShaderResourceView* view = m_textures->at(textureIndex)->GetView();

			context->PSSetShaderResources(0, 1, &view);
		}

		if (m_materials->at(matIndex)->HasBumpMap())
		{
			int bumpIndex = m_materials->at(matIndex)->BumpMapIndex();

			ID3D11ShaderResourceView* view = m_textures->at(bumpIndex)->GetView();

			context->PSSetShaderResources(1, 1, &view);
		}

		int indexStart = m_boidsMesh->GetSubSetStart()->at(j);
		int drawAmount = m_boidsMesh->GetSubSetStart()->at(j + 1) - m_boidsMesh->GetSubSetStart()->at(j);

		context->DrawIndexedInstanced(drawAmount, m_renderAmount, indexStart, 0, 0);
		//m_context->DrawIndexed(drawAmount, indexStart, 0);
	}
}

void SimpleBOIDS::SetRenderCamera(Camera* camera)
{
	m_camera = camera;
}

void SimpleBOIDS::SetTextures(std::vector<SimpleTexture*>* textures)
{
	m_textures = textures;
}

void SimpleBOIDS::SetMaterials(std::vector<Material*>* materials)
{
	m_materials = materials;
}

void SimpleBOIDS::SetSampler(ID3D11SamplerState* state)
{
	m_sampler = state;
}

void SimpleBOIDS::SetBounds(RECT bounds)
{
	m_boundingRect = bounds;
}

void SimpleBOIDS::SetAvoidObject(Gameobject* object)
{
	for (int i = 0; i < NUM_BOIDS; i++)
	{
		m_boids[i]->SetObjectToAvoid(object);
	}
}

HRESULT SimpleBOIDS::Initialise(ID3D11Device* device, ID3D11DeviceContext* context, ID3D11Buffer* objectBuffer, ID3D11Buffer* materialBuffer, Mesh* mesh)
{
	HRESULT result;
	//m_context = context;
	m_materialBuffer = materialBuffer;
	m_perObjectBuffer = objectBuffer;
	//m_perObjectBuffer->AddRef();
	m_boidsMesh = mesh;

	CalculateBoundingSphere();

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },		
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },

//		{ "INSTANCEPOS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
	};

	UINT inputNum = ARRAYSIZE(layout);

	D3D11_BUFFER_DESC indexBufferDesc;
	ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(USHORT) * m_boidsMesh->GetIndexCount();
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA data;
	ZeroMemory(&data, sizeof(D3D11_SUBRESOURCE_DATA));
	data.pSysMem = &m_boidsMesh->GetIndices()[0];
	result = device->CreateBuffer(&indexBufferDesc, &data, &m_boidsIndexBuffer);

#ifdef _DEBUG
	{
		const char name[] = "BOIDS index buffer";
		m_boidsIndexBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	if (FAILED(result))
	{
		return result;
	}

	D3D11_BUFFER_DESC vertexBufferDesc;
	ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));

	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(SimpleVertex) * m_boidsMesh->GetVertexCount();
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA vertexData;
	ZeroMemory(&vertexData, sizeof(vertexData));
	vertexData.pSysMem = &m_boidsMesh->GetVertices()[0];
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_boidsVertexBuffer);

#ifdef _DEBUG
	{
		const char name[] = "BOIDS vertex buffer";
		m_boidsVertexBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	if (FAILED(result))
	{
		return result;
	}

	ID3DBlob* vertexShaderBlob = NULL;
	result = ShaderHelper::CompileShaderFromFile(L"BoidsVS.hlsl", "VS_InstanceBOIDS", "vs_4_0", &vertexShaderBlob);

	if (FAILED(result))
	{
		MessageBox(NULL,
			L"An issue occured with the compiling, try again with the fx file in the same file as the exe",
			L"Error", MB_OK);

		return result;
	}

	result = device->CreateVertexShader(vertexShaderBlob->GetBufferPointer(), vertexShaderBlob->GetBufferSize(), NULL, &m_vertexShader);

	if (FAILED(result))
	{
		vertexShaderBlob->Release();
		return result;
	}

	ID3DBlob* pixelShaderBlob = NULL;

	result = ShaderHelper::CompileShaderFromFile(L"Start of OBJ loader PS.hlsl", "PS_TexturesWithLighting", "ps_4_0", &pixelShaderBlob);
	//result = ShaderHelper::CompileShaderFromFile(L"Start of OBJ loader PS.hlsl", "PS_TexturesNoLighting", "ps_4_0", &pixelShaderBlob);

	if (FAILED(result))
	{
		MessageBox(NULL,
			L"An issue occured with the compiling, try again with the fx file in the same file as the exe",
			L"Error", MB_OK);

		return result;
	}

	result = device->CreatePixelShader(pixelShaderBlob->GetBufferPointer(), pixelShaderBlob->GetBufferSize(), NULL, &m_pixelShader);

#ifdef _DEBUG
	{
		const char name[] = "BOIDS pixel shader";
		m_pixelShader->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	result = device->CreateInputLayout(layout, inputNum, vertexShaderBlob->GetBufferPointer(), vertexShaderBlob->GetBufferSize(), &m_boidsLayout);

	vertexShaderBlob->Release();
	pixelShaderBlob->Release();

	D3D11_BUFFER_DESC instanceBuffer;
	ZeroMemory(&instanceBuffer, sizeof(D3D11_BUFFER_DESC));

	instanceBuffer.Usage = D3D11_USAGE_DEFAULT;
	instanceBuffer.ByteWidth = sizeof(cbBoidsBuffer);
	instanceBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	instanceBuffer.CPUAccessFlags = 0;
	instanceBuffer.MiscFlags = 0;

	result = device->CreateBuffer(&instanceBuffer, NULL, &m_instanceBuffer);

#ifdef _DEBUG
	{
		const char name[] = "BOIDS instance buffer";
		m_instanceBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	SetupBOIDPositions();

	context->UpdateSubresource(m_instanceBuffer, 0, nullptr, &m_boidsBuffer, 0, 0);

	D3D11_RASTERIZER_DESC rsDesc;
	ZeroMemory(&rsDesc, sizeof(rsDesc));
	rsDesc.FillMode = D3D11_FILL_SOLID;
	rsDesc.CullMode = D3D11_CULL_BACK;

	result = device->CreateRasterizerState(&rsDesc, &m_rasterizerState);

	return result;
}

void SimpleBOIDS::Release()
{
	//m_device = nullptr;
	//m_context = nullptr;
	m_boidsMesh = nullptr;
	//m_perObjectBuffer->Release();
	m_boidsIndexBuffer->Release();
	m_boidsVertexBuffer->Release();
	m_instanceBuffer->Release();
	m_boidsLayout->Release();
	m_vertexShader->Release();
	m_pixelShader->Release();
	m_rasterizerState->Release();

	for (int i = 0; i < NUM_BOIDS; i++)
	{
		delete m_boids[i];
	}	

	m_boids.clear();
}

void SimpleBOIDS::SetupBOIDPositions()
{
	srand(100);

	XMMATRIX matRotation;
	XMMATRIX matTemp;

	XMFLOAT3 centrePos = XMFLOAT3(-5, 2, 5);
	XMFLOAT2 spaceSize = XMFLOAT2(0.9f, 2.5f);
	XMFLOAT2 spacePadding = XMFLOAT2(0.5f, 0.5f);

	XMFLOAT2 currentPos = XMFLOAT2(-5.0f, 5.0f);
	int perRow = 10;

	m_boids.reserve(NUM_BOIDS);
	
	for (int i = 0; i < NUM_BOIDS; i++)
	{	
		if (i % perRow == 0 && i != 0)
		{
			currentPos.y += spacePadding.y + spaceSize.y;
			currentPos.x = centrePos.x;
		}

		SBOID data;
		
		data.position.x = currentPos.x;
		data.position.y = centrePos.y;
		data.position.z = currentPos.y;

		data.rotation.x = 0.0f;
		data.rotation.y = 0.0f;
		data.rotation.z = 0.0f;

		data.scale.x = 1.0f;
		data.scale.y = 1.0f;
		data.scale.z = 1.0f;

		m_boids.push_back(new Boid());
		m_boids[i]->Init(data.position, data.rotation, data.scale);		
		
		currentPos.x += spaceSize.x + spacePadding.x;

		XMMATRIX scale = XMMatrixScaling(data.scale.x, data.scale.y, data.scale.z);
		XMMATRIX position = XMMatrixTranslation(data.position.x, data.position.y, data.position.z);

		matTemp = scale * position;

		m_boidsBuffer.info[i] = XMMatrixTranspose(matTemp);
	}
	
	for (int i = 0; i < NUM_BOIDS; i++)
	{
		m_boids[i]->SetBOIDArray(&m_boids);
	}
}

void SimpleBOIDS::UpdateBOIDBuffer(ID3D11DeviceContext* context)
{
	XMMATRIX matTemp;

	BoundingSphere current = m_boidsBoundingSphere;

	int boidIndex = 0;
	m_renderAmount = 0;

	for (int i = 0; i < NUM_BOIDS; i++)
	{	
		current.centre = m_boids[i]->GetPosition();

		if (!m_camera->CullObject(current))
		{
			matTemp = m_boids[i]->GetWorldMatrix();
			m_boidsBuffer.info[boidIndex] = XMMatrixTranspose(matTemp);
			boidIndex++;
			m_renderAmount++;
		}		
	}

	context->UpdateSubresource(m_instanceBuffer, 0, nullptr, &m_boidsBuffer, 0, 0);

	//std::string aString;

	//aString = std::to_string(m_renderAmount) + "\n";

	//OutputDebugStringA(aString.c_str());
}

void SimpleBOIDS::CalculateBoundingSphere()
{
	m_boidsBoundingSphere.centre = XMFLOAT3(0.0f, 0.0f, 0.0f);

	int numberVertices = m_boidsMesh->GetVertexCount();
	SimpleVertex* vertices = m_boidsMesh->GetVertices();

	XMFLOAT3 minPosition = XMFLOAT3(FLT_MAX, FLT_MAX, FLT_MAX);
	XMFLOAT3 maxPosition = XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	for (int i = 0; i < numberVertices; i++)
	{
		SimpleVertex vertex = vertices[i];

		minPosition.x = min(minPosition.x, vertex.Pos.x);
		minPosition.y = min(minPosition.y, vertex.Pos.y);
		minPosition.z = min(minPosition.z, vertex.Pos.z);

		maxPosition.x = max(maxPosition.x, vertex.Pos.x);
		maxPosition.y = max(maxPosition.y, vertex.Pos.y);
		maxPosition.z = max(maxPosition.z, vertex.Pos.z);
	}

	float diameter;

	float dx = maxPosition.x - minPosition.x;
	float dy = maxPosition.y - minPosition.y;
	float dz = maxPosition.z - minPosition.z;

	diameter = sqrt(dx * dx + dy * dy + dz * dz);

	m_boidsBoundingSphere.radius = diameter / 2;
}

//Not complete yet
//Sort out the positions of each boid using buffers
//fixed the glitch, the layout was incorrect