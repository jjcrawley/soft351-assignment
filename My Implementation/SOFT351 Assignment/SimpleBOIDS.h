#pragma once
#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>
#include "Mesh.h"
#include "ShaderHelper.h"
#include "Renderer.h"
#include "Camera.h"
#include "Boid.h"

#define NUM_BOIDS 50

struct SBOID
{
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 rotation;
	DirectX::XMFLOAT3 scale;
};

struct cbBoidsBuffer
{
	DirectX::XMMATRIX info[NUM_BOIDS];
};

#ifndef BOIDS

#define BOIDS

class SimpleBOIDS
{
public:
	void Update(float delta);
	void Render(ID3D11DeviceContext* context);
	void SetRenderCamera(Camera* camera);
	void SetTextures(std::vector<SimpleTexture*>* textures);
	void SetMaterials(std::vector<Material*>* materials);
	void SetSampler(ID3D11SamplerState* state);
	void SetBounds(RECT bounds);
	void SetAvoidObject(Gameobject* object);
	HRESULT Initialise(ID3D11Device* device, ID3D11DeviceContext* context, ID3D11Buffer* objectBuffer, ID3D11Buffer* materialBuffer, Mesh* mesh);
	void Release();
private:	
	ID3D11Buffer* m_boidsVertexBuffer;
	ID3D11Buffer* m_boidsIndexBuffer;
	ID3D11Buffer* m_instanceBuffer;
	ID3D11Buffer* m_perObjectBuffer;
	ID3D11Buffer* m_materialBuffer;
	ID3D11InputLayout* m_boidsLayout;
	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11RasterizerState* m_rasterizerState;
	ID3D11SamplerState*	m_sampler;
	std::vector<SimpleTexture*>* m_textures;
	std::vector<Material*>* m_materials;
	cbBoidsBuffer m_boidsBuffer;	
	Camera* m_camera;
	Mesh* m_boidsMesh;
	BoundingSphere m_boidsBoundingSphere;
	RECT m_boundingRect = {-30, 30, 30, -30};
	std::vector<Boid*> m_boids;
	int m_renderAmount = 0;
	void SetupBOIDPositions();
	void UpdateBOIDBuffer(ID3D11DeviceContext* context);
	void CalculateBoundingSphere();
};

#endif // !BOIDS

//TODO: extract necessary rendering code from particle system, use models
//TODO: create rules for boids
//TODO: apply