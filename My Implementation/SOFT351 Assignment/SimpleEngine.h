#pragma once
#include <windows.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dcompiler.h>
#include <xnamath.h>
#include <fstream>		// Files.  The ones without ".h" are the new (not
#include <string>		// so new now) standard library headers.
#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <dinput.h>
#include "resource.h"
#include "Input.h"
#include "Material.h"
#include "Renderer.h"
#include "Mesh.h"

class SimpleEngine
{
public:
	SimpleEngine();
	~SimpleEngine();
	HRESULT Initialise();
	void UpdateEngine(float time);
	void Render();
	Mesh* LoadMesh(LPSTR filename, bool LHS);
	void LoadMeshInternal(LPSTR filename, bool LHS);
	void Release();
private:
	std::vector<Material*>	m_materials;
	std::vector<Mesh*>		m_meshes;
	std::vector<Renderer*>	m_renderers;
	std::vector<SimpleTexture*> m_textures;
};