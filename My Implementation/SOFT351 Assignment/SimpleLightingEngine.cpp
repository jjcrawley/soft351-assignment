#include "SimpleLightingEngine.h"

typedef DirectX::XMFLOAT3 XMFLOAT3;
typedef DirectX::XMFLOAT4 XMFLOAT4;

SimpleLightingEngine::SimpleLightingEngine()
{
	m_globalAmbient = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
}

HRESULT SimpleLightingEngine::Initialise(ID3D11Device* device, ID3D11DeviceContext* context)
{
	m_context = context;

	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(bufferDesc));
	bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.ByteWidth = sizeof(CBLightProperties);
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.MiscFlags = 0;

	HRESULT result = device->CreateBuffer(&bufferDesc, NULL, &m_lightBuffer);

#ifdef _DEBUG
	{
		const char name[] = "Lighting buffer";
		m_lightBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	return result;
}

Light* SimpleLightingEngine::AddAndCreateLight()
{
	Light* light = new Light();
	
	light->SetEnabled(true);
	light->SetConeAngle(DirectX::XMConvertToRadians(45.0f));
	light->SetDirection(XMFLOAT3(1.0f, 0.0f, 0.0f));
	light->SetDiffuse(XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
	light->SetRange(200.0f);
	light->SetAttenuation(XMFLOAT3(0.4f, 0.02f, 0.0f));
	light->SetLightType(Directional);

	m_lights.push_back(light);

	return light;
}

void SimpleLightingEngine::SetGlobalAmbient(XMFLOAT4 ambient)
{
	m_globalAmbient = ambient;
}

void SimpleLightingEngine::SetEyePosition(XMFLOAT3 position)
{
	m_eyePosition = XMFLOAT4(position.x, position.y, position.z, 1.0f);
}

XMFLOAT4 SimpleLightingEngine::GetGlobalAmbient()
{
	return m_globalAmbient;
}

void SimpleLightingEngine::Update()
{
	CBLightProperties light;	

	for (int i = 0; i < m_lights.size(); i++)
	{
		LightInfo info = m_lights[i]->GetLightInfo();

		light.lights[i] = info;
	}

	light.globalAmbient = m_globalAmbient;
	light.eyePosition = m_eyePosition;
	light.numberLights = (int)m_lights.size();

	m_context->UpdateSubresource(m_lightBuffer, 0, NULL, &light, 0, 0);
	m_context->PSSetConstantBuffers(3, 1, &m_lightBuffer);
}

void SimpleLightingEngine::ReleaseLight(int index)
{
	if (index < m_lights.size() && index >= 0)
	{
		Light* light = m_lights[index];
		m_lights.erase(m_lights.begin()+index);
		delete light;
	}
}

Light* SimpleLightingEngine::GetLight(int index)
{
	return m_lights[index];
}

void SimpleLightingEngine::Release()
{
	if (m_lightBuffer)
	{
		m_lightBuffer->Release();
		m_lightBuffer = nullptr;
	}	

	for (int i = 0; i < m_lights.size(); i++)
	{
		delete m_lights[i];
	}
}