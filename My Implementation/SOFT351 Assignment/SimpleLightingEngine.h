#pragma once
#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>
#include "Light.h"

#define MAX_NUMBER_LIGHTS 8

struct CBLightProperties
{
	DirectX::XMFLOAT4 globalAmbient;
	DirectX::XMFLOAT4 eyePosition;
	LightInfo lights[MAX_NUMBER_LIGHTS];
	int numberLights;
	DirectX::XMFLOAT3 padding;
};

#ifndef SIMPLE_LIGHTING_ENGINE
#define SIMPLE_LIGHTING_ENGINE

class SimpleLightingEngine
{
public:
	SimpleLightingEngine();
	HRESULT Initialise(ID3D11Device* device, ID3D11DeviceContext* context);
	Light* AddAndCreateLight();
	void SetGlobalAmbient(DirectX::XMFLOAT4 ambient);
	void SetEyePosition(DirectX::XMFLOAT3 position);
	DirectX::XMFLOAT4 GetGlobalAmbient();
	void Update();
	void ReleaseLight(int index);
	Light* GetLight(int index);
	void Release();
private:
	ID3D11DeviceContext* m_context;
	ID3D11Buffer* m_lightBuffer;	
	DirectX::XMFLOAT4 m_globalAmbient;
	DirectX::XMFLOAT4 m_eyePosition;
	std::vector<Light*> m_lights;
};

#endif // !SIMPLE_LIGHTING_ENGINE