#pragma once
#include "Gameobject.h"

#ifndef SIMPLEOBJ
#define SIMPLEOBJ

class SimpleObject : 
	public Gameobject
{
public:
	virtual void Update(float delta) override;
};

#endif // !SIMPLEOBJ