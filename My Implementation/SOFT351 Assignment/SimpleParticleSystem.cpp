#include "SimpleParticleSystem.h"

using namespace DirectX;

void SimpleParticleSystem::Render(ID3D11DeviceContext* context, float delta)
{
	UINT strides = sizeof(SimpleVertex);
	UINT offsets = 0;

	ID3D11Buffer* instanceBuffers[1] = {m_particleVertexBuffer};

	context->IASetInputLayout(m_particleLayout);

	context->IASetIndexBuffer(m_particleIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	context->IASetVertexBuffers(0, 1, instanceBuffers, &strides, &offsets);

	context->PSSetShader(m_pixelShader, NULL, 0);
	context->VSSetShader(m_vertexShader, NULL, 0);

	static float angle;
	angle += delta;

	XMMATRIX matWorldPos = XMMatrixTranslation(0.0f, 0.0f, 0.0f);
	XMMATRIX matRotate = XMMatrixRotationY(angle);

	XMMATRIX matWorld = matRotate * matWorldPos;

	XMMATRIX matWVP = matWorld * m_camera->GetViewMatrix() * m_camera->GetProjectionMatrix();
	
	CBChangesEveryFrame buffer;
	buffer.matWorld = XMMatrixTranspose(matWorldPos);
	buffer.matWorldViewProjection = XMMatrixTranspose(matWVP);

	context->UpdateSubresource(m_perObjectBuffer, 0, NULL, &buffer, 0, 0);

	ID3D11Buffer* vsConstantBuffers[2] = {m_perObjectBuffer, m_instanceBuffer};
	context->VSSetConstantBuffers(1, 2, vsConstantBuffers);
	context->PSSetConstantBuffers(1, 1, &m_perObjectBuffer);
	context->PSSetConstantBuffers(4, 1, &m_colourBuffer);
	context->RSSetState(m_rasterizerState);

	context->DrawIndexedInstanced(6, NUM_PARTICLES, 0, 0, 0);
}

void SimpleParticleSystem::SetRenderCamera(Camera* camera)
{
	m_camera = camera;
}

HRESULT SimpleParticleSystem::Initialise(ID3D11Device* device, ID3D11DeviceContext* context, ID3D11Buffer* objectBuffer)
{
	HRESULT result;
	//m_device = device;

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0},

//		{ "INSTANCEPOS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1}
	};

	UINT inputNum = ARRAYSIZE(layout);	

	//construct a simple quad
	SimpleVertex vertexes[] =
	{
		SimpleVertex(-1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f),  //bottom left corner
		SimpleVertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f),  //top left corner
		SimpleVertex( 1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f),  //top right corner
		SimpleVertex( 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f),  //bottom right corner
	};

	//construct the quad indices
	DWORD indices[]
	{
		0, 1, 2,
		0, 2, 3,
	};

	D3D11_BUFFER_DESC indexBufferDesc;
	ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(DWORD) * 2 * 3;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA data;

	data.pSysMem = indices;
	result = device->CreateBuffer(&indexBufferDesc, &data, &m_particleIndexBuffer);

	if (FAILED(result))
	{
		return result;
	}

#ifdef _DEBUG
	{
		const char name[] = "Particle index buffer";
		m_particleIndexBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	D3D11_BUFFER_DESC vertexBufferDesc;
	ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));

	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(SimpleVertex) * 4;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA vertexData;
	ZeroMemory(&vertexData, sizeof(vertexData));
	vertexData.pSysMem = vertexes;
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_particleVertexBuffer);

	if (FAILED(result))
	{
		return result;
	}

#ifdef _DEBUG
	{
		const char name[] = "Particle Vertex buffer";
		m_particleVertexBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	ID3DBlob* vertexShaderBlob = NULL;
	result = ShaderHelper::CompileShaderFromFile(L"InstanceVertexShader.hlsl", "VS_Instance", "vs_4_0", &vertexShaderBlob);

	if (FAILED(result))
	{
		MessageBox(NULL, 
			L"An issue occured with the compiling, try again with the fx file in the same file as the exe",
			L"Error", MB_OK);

		return result;
	}

	result = device->CreateVertexShader(vertexShaderBlob->GetBufferPointer(), vertexShaderBlob->GetBufferSize(), NULL, &m_vertexShader);
	
	if (FAILED(result))
	{
		vertexShaderBlob->Release();
		return result;
	}

	ID3DBlob* pixelShaderBlob = NULL;

	result = ShaderHelper::CompileShaderFromFile(L"Start of OBJ loader PS.hlsl", "PS_Particles", "ps_4_0", &pixelShaderBlob);

	if (FAILED(result))
	{
		MessageBox(NULL,
			L"An issue occured with the compiling, try again with the fx file in the same file as the exe",
			L"Error", MB_OK);

		return result;
	}

	result = device->CreatePixelShader(pixelShaderBlob->GetBufferPointer(), pixelShaderBlob->GetBufferSize(), NULL, &m_pixelShader);

	if (FAILED(result))
	{
		MessageBox(NULL,
			L"An issue occured with the compiling, try again with the fx file in the same file as the exe",
			L"Error", MB_OK);

		return result;
	}

#ifdef _DEBUG
	{
		const char name[] = "Particle pixel shader";
		m_pixelShader->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	result = device->CreateInputLayout(layout, inputNum, vertexShaderBlob->GetBufferPointer(), vertexShaderBlob->GetBufferSize(), &m_particleLayout);

	vertexShaderBlob->Release();
	pixelShaderBlob->Release();	

	D3D11_BUFFER_DESC instanceBuffer;
	ZeroMemory(&instanceBuffer, sizeof(D3D11_BUFFER_DESC));

	instanceBuffer.Usage = D3D11_USAGE_DEFAULT;
	instanceBuffer.ByteWidth = sizeof(cbPerSystem);
	instanceBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	instanceBuffer.CPUAccessFlags = 0;
	instanceBuffer.MiscFlags = 0;

	result = device->CreateBuffer(&instanceBuffer, NULL, &m_instanceBuffer);

	if (FAILED(result))
	{
		return result;
	}

#ifdef _DEBUG
	{
		const char name[] = "Particle instance buffer";
		m_instanceBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	SetupParticlePositions();

	context->UpdateSubresource(m_instanceBuffer, 0, nullptr, &m_systemBuffer, 0, 0);

	m_perObjectBuffer = objectBuffer;

	D3D11_BUFFER_DESC colourBuffer;
	ZeroMemory(&colourBuffer, sizeof(D3D11_BUFFER_DESC));

	colourBuffer.Usage = D3D11_USAGE_DEFAULT;
	colourBuffer.ByteWidth = sizeof(ParticleColour);
	colourBuffer.CPUAccessFlags = 0;
	colourBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	colourBuffer.MiscFlags = 0;

	result = device->CreateBuffer(&colourBuffer, NULL, &m_colourBuffer);

	if (FAILED(result))
	{
		return result;
	}

#ifdef _DEBUG
	{
		const char name[] = "Particle colour buffer";
		m_colourBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	ParticleColour colour;
	colour.colour = XMFLOAT4(0.07568f, 0.61424f, 0.07568f, 1.0f);
	//colour.colour = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	context->UpdateSubresource(m_colourBuffer, 0, nullptr, &colour, 0, 0);

	D3D11_RASTERIZER_DESC rsDesc;
	ZeroMemory(&rsDesc, sizeof(D3D11_RASTERIZER_DESC));
	rsDesc.FillMode = D3D11_FILL_SOLID;
	rsDesc.CullMode = D3D11_CULL_NONE;

	result = device->CreateRasterizerState(&rsDesc, &m_rasterizerState);

	return result;
}

void SimpleParticleSystem::Release()
{
	m_perObjectBuffer = NULL;
	m_particleIndexBuffer->Release();
	m_instanceBuffer->Release();
	m_colourBuffer->Release();
	m_particleLayout->Release();
	m_particleVertexBuffer->Release();
	m_vertexShader->Release();
	m_pixelShader->Release();
	m_rasterizerState->Release();
}

void SimpleParticleSystem::SetupParticlePositions()
{
	srand(100);

	XMFLOAT3 tempPos;
	XMMATRIX matRotation;
	XMMATRIX matTemp;

	float radius = 30.0f;

	for(int i = 0; i < NUM_PARTICLES; i++)
	{
		XMFLOAT3 rotation;

		rotation.x = (rand() % 3000) / 500.0f;
		rotation.y = (rand() % 3000) / 500.0f;
		rotation.z = (rand() % 3000) / 500.0f;

		float distance = radius - (rand() % 100) / 250.0f;

		ParticleData data;
		data.position = XMFLOAT3(distance, 0.0f, 0.0f);
		
		m_particlePositions[i] = data;

		XMVECTOR pos = XMVectorSet(distance, 0.0f, 0.0f, 0.0f);
		matRotation = XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z);
		pos = XMVector3TransformCoord(pos, matRotation);

		XMStoreFloat3(&tempPos, pos);

		XMMATRIX scale = XMMatrixScaling(0.25f, 0.25f, 0.25f);
		XMMATRIX position = XMMatrixTranslation(tempPos.x, tempPos.y, tempPos.z);

		matTemp = scale * matRotation * position;

		m_systemBuffer.particlePosition[i] = XMMatrixTranspose(matTemp);
	}
}