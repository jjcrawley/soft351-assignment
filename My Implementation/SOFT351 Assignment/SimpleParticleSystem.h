#pragma once
#include <d3d11.h>
#include <DirectXMath.h>
#include "Mesh.h"
#include "ShaderHelper.h"
#include "Renderer.h"
#include "Camera.h"

#define NUM_PARTICLES 500

struct cbPerSystem
{
	DirectX::XMMATRIX particlePosition[NUM_PARTICLES];
};

struct ParticleData
{
	DirectX::XMFLOAT3 position;
};

struct ParticleColour
{
	DirectX::XMFLOAT4 colour;
};

#ifndef SIMPLE_PARTICLE_SYSTEM

#define SIMPLE_PARTICLE_SYSTEM

class SimpleParticleSystem
{
public:
	void Render(ID3D11DeviceContext* m_context, float delta);
	void SetRenderCamera(Camera* camera);
	HRESULT Initialise(ID3D11Device* device, ID3D11DeviceContext* context, ID3D11Buffer* objectBuffer);
	void Release();
private:
	ID3D11Buffer* m_particleVertexBuffer;
	ID3D11Buffer* m_particleIndexBuffer;
	ID3D11Buffer* m_instanceBuffer;
	ID3D11Buffer* m_perObjectBuffer;
	ID3D11Buffer* m_colourBuffer;
	ID3D11InputLayout* m_particleLayout;
	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11RasterizerState* m_rasterizerState;
	cbPerSystem m_systemBuffer;
	Camera* m_camera;
	ParticleData m_particlePositions[NUM_PARTICLES];
	void SetupParticlePositions();
};

#endif // !SIMPLE_PARTICLE_SYSTEM