#include "SimpleTexture.h"

void SimpleTexture::SetName(std::wstring name)
{
	m_textureName = name;
}

std::wstring SimpleTexture::GetName()
{
	return m_textureName;
}

void SimpleTexture::SetShaderResource(ID3D11ShaderResourceView* view)
{
	m_view = view;
}

ID3D11ShaderResourceView* SimpleTexture::GetView()
{
	return m_view;
}

void SimpleTexture::Release()
{
	m_view->Release();
	m_view = nullptr;
}