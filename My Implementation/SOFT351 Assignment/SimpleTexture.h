#pragma once
#include <d3d11.h>
#include <string>

#ifndef SIMPLETEXTURE
#define SIMPLETEXTURE
class SimpleTexture
{
public:
	void SetName(std::wstring name);
	std::wstring GetName();
	void SetShaderResource(ID3D11ShaderResourceView* view);
	ID3D11ShaderResourceView* GetView();
	void Release();
private:
	std::wstring m_textureName;
	ID3D11ShaderResourceView* m_view;
};
#endif