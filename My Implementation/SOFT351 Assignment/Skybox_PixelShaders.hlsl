#define MAX_NUMBER_LIGHTS 8

struct Light
{
	float3 direction;
	float cone;
	float4 diffuseCol;
	float3 position;
	float range;
	float3 attenuation;
	int lightType;
	bool enabled;
	float3 padding;
};

cbuffer cbMaterial : register(b0)
{
	float4 materialColour;
	float4 ambientColour;
	float4 materialSpecular;
	float specularPower;
	bool hasTexture;
	bool hasBumpMap;
	float padding;
};

cbuffer cbLightingProperties : register(b3)
{
	float4 globalAmbience;

	float4 eyePosition;

	Light sceneLights[MAX_NUMBER_LIGHTS];

	int numberLights;
	float3 paddingStuff;
};

cbuffer cbSkyBox : register(b5)
{
	float4 skyColour;
	float time;
	float3 crawlerPos;
};

Texture2D    txDiffuse : register(t0);
Texture2D    txBumpMap : register(t1);
SamplerState samLinear : register(s0);

float screenHeight = 480;
float screenWidth = 640;
float pi = 3.14159265f;

struct PS_INPUT
{
	float4 Pos		 : SV_POSITION;
	float4 WorldPos  : POSITION;
	float3 vecNormal : NORMAL;
	float3 tangent   : TANGENT;
	float2 Tex		 : TEXCOORD;
};

float4 PS_SkyboxBasic(PS_INPUT input) : SV_Target
{
	float4 diffuse = materialColour;

	if (hasTexture)
		diffuse = diffuse * txDiffuse.Sample(samLinear, input.Tex);

	return diffuse;
}

float4 PS_SkyboxWibble(PS_INPUT input) : SV_Target
{
	float4 diffuse = float4(0, 0, 0, 1.0);

	float speed = time * 0.001;
	float frequency = 10;
	float amplitude = 0.1;

	float2 tex = input.Tex;

	tex.y += sin(tex.x * frequency + speed) * amplitude;

	if (hasTexture)
		diffuse = txDiffuse.Sample(samLinear, tex);

	diffuse *= skyColour;
	diffuse *= float4(2, 2, 2, 1.0f);

	diffuse = saturate(diffuse);

	return diffuse;
}

float4 LerpOnDistance(float r, float g, float b, float distance)
{
	float4 diffuse = float4(0, 0, 0, 1.0f);

	diffuse.x = lerp(0, r, distance);
	diffuse.y = lerp(0, g, distance);
	diffuse.z = lerp(0, b, distance);

	return diffuse;
}

float4 PS_SkyboxColour(PS_INPUT input) : SV_Target
{
	float4 diffuse = float4(0, 0, 0, 1.0);

	float speed = time * 0.01;
	float frequency = 0.3;
	float amplitude = 5;

	float height = screenHeight + sin(input.Pos.x * frequency + speed) * amplitude;

	float distance = abs(input.Pos.y - height) / 500;

	diffuse = LerpOnDistance(0.2, 0.3, 0.5, distance);

	diffuse.xyz += cos(speed * 0.1) * 0.1;

	diffuse = saturate(diffuse);

	return diffuse;
}

float4 PS_SkyboxCrawler(PS_INPUT input) : SV_Target
{
	float4 diffuse = float4(0, 0, 0, 1.0);

	float speed = time * 0.01;
	float frequency = 0.3;
	float amplitude = 5;

	float height = 0;
	float dist = 0;

	if (input.vecNormal.y > 0 || input.vecNormal.y < 0)
	{
		//at the bottom or top face
		float2 facePos = input.WorldPos.xz;

		float2 centre = crawlerPos.xz;

		float radius = 10.0 + sin(speed * 0.1 + frequency) * 2.0f;

		float2 difference = facePos - centre;

		float2 pointOnCircle = centre + normalize(difference) * radius;

		float length = distance(pointOnCircle, facePos);

		//the basic setup, inversely proportional to distance
		diffuse = LerpOnDistance(0.2, 0.3, 0.5, 5 / length);

		//uncomment to see large death like circle
		//diffuse = LerpOnDistance(0.2, 0.3, 0.5, length/500);

		//uncomment to see the other effect, colour ring
		//diffuse *= skyColour;
	}
	else if (input.vecNormal.z > 0)
	{
		//at the left face
		height = eyePosition.y + sin(input.WorldPos.x * frequency + speed) * amplitude;

		dist = abs(input.WorldPos.y - height) / 500;

		diffuse = LerpOnDistance(0.2, 0.3, 0.5, dist);
	}
	else if (input.vecNormal.z < -0.00001)
	{
		//at the right face
		height = eyePosition.y + sin((input.WorldPos.x * frequency - speed)) * amplitude;

		dist = abs(input.WorldPos.y - height) / 500;

		diffuse = LerpOnDistance(0.2, 0.3, 0.5, dist);
	}
	else if (input.vecNormal.x > 0.00001)
	{
		//at the front face
		height = eyePosition.y + sin((input.WorldPos.z * frequency - speed)) * amplitude;

		dist = abs(input.WorldPos.y - height) / 500;

		diffuse = LerpOnDistance(0.2, 0.3, 0.5, dist);
	}
	else if (input.vecNormal.x < 0)
	{
		//at the rear face
		height = eyePosition.y + sin((input.WorldPos.z * frequency + speed)) * amplitude;

		dist = abs(input.WorldPos.y - height) / 500;

		diffuse = LerpOnDistance(0.2, 0.3, 0.5, dist);
	}

	diffuse.xyz += cos(speed * 0.1) * 0.1;

	diffuse = saturate(diffuse);

	return diffuse;
}