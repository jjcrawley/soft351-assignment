//**************************************************************************//
// Start of an OBJ loader.  By no means an end.  This just creates			//
// triangles.																//
//																			//
// Look for the Nigel style comments, like these, for the bits you need to  //
// look at.																	//
//**************************************************************************//

//**************************************************************************//
// Modifications to the MS sample code is copyright of Dr Nigel Barlow,		//
// lecturer in computing, University of Plymouth, UK.						//
// email: nigel@soc.plymouth.ac.uk.											//
//																			//
// You may use, modify and distribute this (rather cack-handed in places)	//
// code subject to the following conditions:								//
//																			//
//	1:	You may not use it, or sell it, or use it in any adapted form for	//
//		financial gain, without my written premission.						//
//																			//
//	2:	You must not remove the copyright messages.							//
//																			//
//	3:	You should correct at least 10% of the typig abd spekking errirs.   //
//**************************************************************************//

//--------------------------------------------------------------------------------------
// File: Tutorial07 - Textures and Constant buffers.cpp
//
// This application demonstrates texturing
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
#pragma once
#include <d3d11.h>
#include <dxgi.h>
#include <dxgidebug.h>
//#include <d3dx11.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <iostream>
#include <vector>
#include <string>
#include "resource.h"
#include "Input.h"
#include "Mesh.h"
#include "Material.h"
#include "Renderer.h"
#include "SimpleTexture.h"
#include "Gameobject.h"
#include "MoveableObject.h"
#include "SimpleObject.h"
#include "Camera.h"
#include "RotatingObject.h"
#include "Timer.h"
#include "Light.h"
#include "SimpleLightingEngine.h"
#include "MeshLoader.h"
#include "FollowCamera.h"
#include "ShaderHelper.h"
#include "SimpleParticleSystem.h"
#include "SimpleBOIDS.h"
#include "Crawler.h"

//**************************************************************************//
// Global Variables.  There are many global variables here (we aren't OO	//
// yet.  I doubt  Roy Tucker (Comp Sci students will know him) will			//
// approve pf this either.  Sorry, Roy.										//
//**************************************************************************//
HINSTANCE                           g_hInst = NULL;
HWND                                g_hWnd = NULL;
D3D_DRIVER_TYPE                     g_driverType = D3D_DRIVER_TYPE_NULL;
D3D_FEATURE_LEVEL                   g_featureLevel = D3D_FEATURE_LEVEL_11_0;
ID3D11Device*                       g_pd3dDevice = NULL;
ID3D11DeviceContext*                g_pImmediateContext = NULL;
IDXGISwapChain*                     g_pSwapChain = NULL;
ID3D11RenderTargetView*             g_pRenderTargetView = NULL;
ID3D11Texture2D*                    g_pDepthStencil = NULL;
ID3D11DepthStencilView*             g_pDepthStencilView = NULL;
ID3D11VertexShader*                 g_pVertexShader = NULL;
ID3D11PixelShader*                  g_pPixelShader = NULL;
ID3D11InputLayout*                  g_pVertexLayout = NULL;
ID3D11SamplerState*                 g_pSamplerLinear = NULL;

std::vector<SimpleTexture*> g_textures;

//**************************************************************************//
// Now a global instance of each constant buffer.							//
//**************************************************************************//
ID3D11Buffer                       *g_pCBMaterialBuffer      = NULL;
ID3D11Buffer                       *g_pCBChangeOnResize    = NULL;
ID3D11Buffer                       *g_pCBChangesEveryFrame = NULL;

SimpleLightingEngine*				g_lightingEngine;
Light*								g_testLight;

ID3D11RasterizerState*				g_pRasterizerState;
std::vector<Material*>			    g_materials;
std::vector<Mesh*>		g_meshes;
std::vector<Renderer*>	g_renderers;

Input*								g_input;
Timer*								g_timer;

Camera*								g_camera;
std::vector<Gameobject*>			g_objects;
MoveableObject*						g_theDeer;
SimpleParticleSystem*				g_particleSystem;
SimpleBOIDS*						g_boidsSimulation;

Gameobject*							g_skybox;
ID3D11PixelShader*					g_skyPixelWibble = NULL;
ID3D11PixelShader*					g_skyNoEffect = NULL;
ID3D11PixelShader*					g_skyColourEffect = NULL;
ID3D11PixelShader*					g_skyboxCrawler = NULL;
ID3D11PixelShader*					g_currentSkybox = NULL;
ID3D11Buffer*						g_skyBoxBuffer = NULL;
CBSkybox							g_skyBoxInfo;
Crawler*							g_crawler;


//**************************************************************************//
// Forward declarations.													//
//																			//
// If you are not used to "C" you will find that functions (or methods in	//
// "C++" must have templates defined in advance.  It is usual to define the //
// prototypes in a header file, but we'll put them here for now to keep		//
// things simple.															//
//**************************************************************************//
HRESULT InitWindow( HINSTANCE hInstance, int nCmdShow );
HRESULT InitDevice();
HRESULT InitSkyBox();
HRESULT InitObjects();
HRESULT InitBOIDS();
HRESULT InitLighting();
void CleanupDevice();
Mesh* LoadMesh(LPSTR meshFilename, bool LHS);
LRESULT CALLBACK    WndProc( HWND, UINT, WPARAM, LPARAM );
void Render();
void charStrToWideChar(WCHAR *dest, char *source);
MoveableObject* CreateMoveableObject(LPSTR meshFilename, bool LHS);
SimpleObject* CreateSimpleObject(LPSTR meshFilename, bool LHS);
RotatingObject* CreateRotatingObject(LPSTR meshFilename, bool LHS);
void CheckSound();
void CheckExit();
void UpdateLight();
void Update();

//**************************************************************************//
// A Windows program always kicks off in WinMain.							//
// Initializes everything and goes into a message processing				//
// loop. Idle time is used to render the scene.								//
//																			//
// In other words, run the computer flat out.  Is this good?				//
//**************************************************************************//
int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
    UNREFERENCED_PARAMETER( hPrevInstance );
    UNREFERENCED_PARAMETER( lpCmdLine );

    if( FAILED( InitWindow( hInstance, nCmdShow ) ) )
        return 0;

    if( FAILED( InitDevice() ) )
    {
        CleanupDevice();
        return 0;
    }

	//**************************************************************************//
    // Main Windoze message loop.												//
	//																			//
	// Gamers will see this as a game loop, though you will find something like //
	// this main loop deep within any Windows application.						//
	//**************************************************************************//
    MSG msg = {0};
    while( WM_QUIT != msg.message )
    {
        if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
        {
            TranslateMessage( &msg );
            DispatchMessage( &msg );
        }
        else
        {
			Update();
            Render();
        }
    }

    CleanupDevice();

    return ( int )msg.wParam;
}

//--------------------------------------------------------------------------------------
// Register class and create window
//--------------------------------------------------------------------------------------
HRESULT InitWindow( HINSTANCE hInstance, int nCmdShow )
{
    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof( WNDCLASSEX );
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon( hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
    wcex.hCursor = LoadCursor( NULL, IDC_ARROW );
    wcex.hbrBackground = ( HBRUSH )( COLOR_WINDOW + 1 );
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = L"TutorialWindowClass";
    wcex.hIconSm = LoadIcon( wcex.hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
    if( !RegisterClassEx( &wcex ) )
        return E_FAIL;

    // Create window
    g_hInst = hInstance;
    RECT rc = { 0, 0, 640, 480 };
    AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
    g_hWnd = CreateWindow( L"TutorialWindowClass", L"Direct3D 11", WS_OVERLAPPEDWINDOW,
                           CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
                           NULL );
    if( !g_hWnd )
        return E_FAIL;

    ShowWindow( g_hWnd, nCmdShow );

    return S_OK;
}

//--------------------------------------------------------------------------------------
// Create Direct3D device and swap chain
//--------------------------------------------------------------------------------------
HRESULT InitDevice()
{
    HRESULT hr = S_OK;

    RECT rc;
    GetClientRect( g_hWnd, &rc );
    UINT width = rc.right - rc.left;
    UINT height = rc.bottom - rc.top;

    UINT createDeviceFlags = 0;
#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    D3D_DRIVER_TYPE driverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    UINT numDriverTypes = ARRAYSIZE( driverTypes );

    D3D_FEATURE_LEVEL featureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
    };
    UINT numFeatureLevels = ARRAYSIZE( featureLevels );
	
    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory( &sd, sizeof( sd ) );
    sd.BufferCount = 1;
    sd.BufferDesc.Width = width;
    sd.BufferDesc.Height = height;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = g_hWnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;

    for( UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++ )
    {
        g_driverType = driverTypes[driverTypeIndex];
        hr = D3D11CreateDeviceAndSwapChain( NULL, g_driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
                                            D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, &g_featureLevel, &g_pImmediateContext );
        if( SUCCEEDED( hr ) )
            break;
    }
    if( FAILED( hr ) )
        return hr;

    // Create a render target view
    ID3D11Texture2D* pBackBuffer = NULL;
    hr = g_pSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer );
    if( FAILED( hr ) )
        return hr;

    hr = g_pd3dDevice->CreateRenderTargetView( pBackBuffer, NULL, &g_pRenderTargetView );
    pBackBuffer->Release();
    if( FAILED( hr ) )
        return hr;

    // Create depth stencil texture
    D3D11_TEXTURE2D_DESC descDepth;
    ZeroMemory( &descDepth, sizeof(descDepth) );
    descDepth.Width = width;
    descDepth.Height = height;
    descDepth.MipLevels = 1;
    descDepth.ArraySize = 1;
    descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    descDepth.SampleDesc.Count = 1;
    descDepth.SampleDesc.Quality = 0;
    descDepth.Usage = D3D11_USAGE_DEFAULT;
    descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    descDepth.CPUAccessFlags = 0;
    descDepth.MiscFlags = 0;
    hr = g_pd3dDevice->CreateTexture2D( &descDepth, NULL, &g_pDepthStencil );
    if( FAILED( hr ) )
        return hr;

    // Create the depth stencil view
    D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
    ZeroMemory( &descDSV, sizeof(descDSV) );
    descDSV.Format = descDepth.Format;
    descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    descDSV.Texture2D.MipSlice = 0;
    hr = g_pd3dDevice->CreateDepthStencilView( g_pDepthStencil, &descDSV, &g_pDepthStencilView );
    if( FAILED( hr ) )
        return hr;

    g_pImmediateContext->OMSetRenderTargets( 1, &g_pRenderTargetView, g_pDepthStencilView );

    // Setup the viewport
    D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)width;
    vp.Height = (FLOAT)height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0.0f;
    vp.TopLeftY = 0.0f;
    g_pImmediateContext->RSSetViewports( 1, &vp );
	
	//**********************************************************************//
	// Compile the shader file.  These files aren't pre-compiled (well, not //
	// here, and are compiles on he fly).									//
	//																		//
	// This is DirectX11, but what shader model do you see here?			//
	// Change to shader model 5 in Babbage209 and it should still work.		//
	//**********************************************************************//
    ID3DBlob* pVSBlob = NULL;
    hr = ShaderHelper::CompileShaderFromFile( L"Start of OBJ loader VS.hlsl", "VS_obj", "vs_4_0", &pVSBlob );
    if( FAILED( hr ) )
    {
        MessageBox( NULL,
                    L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK );
        return hr;
    }

	//**********************************************************************//
    // Create the vertex shader.											//
	//**********************************************************************//
	hr = g_pd3dDevice->CreateVertexShader( pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &g_pVertexShader );
    if( FAILED( hr ) )
    {    
        pVSBlob->Release();
        return hr;
    }

	//**********************************************************************//
    // Create the pixel shader.												//
    //**********************************************************************//
	ID3DBlob* pPSBlob = NULL;
	hr = ShaderHelper::CompileShaderFromFile(L"Start of OBJ loader PS.hlsl", "PS_TexturesWithLighting", "ps_4_0", &pPSBlob);
	if( FAILED( hr ) )
    {
        MessageBox( NULL,
                    L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK );
        return hr;
    }
    hr = g_pd3dDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &g_pPixelShader );
    pPSBlob->Release();
    if( FAILED( hr ) )
        return hr;	

#ifdef _DEBUG
	{
		const char name[] = "Main pixel shader";
		g_pPixelShader->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	//**********************************************************************//
    // Define the input layout.  I won't go too much into this except that  //
	// the vertex defined here MUST be consistent with the vertex shader	//
	// input you use in your shader file and the constand buffer structure  //
	// at the top of this module.											//
	//																		//
	// Here a vertex has a position a normal vector (used for lighting) and //
	// a single texture UV coordinate.										//
	//**********************************************************************//
    D3D11_INPUT_ELEMENT_DESC layout[] =
    {
        { "POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL",    0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT,    0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

    UINT numElements = ARRAYSIZE( layout );

    // Create the input layout
    hr = g_pd3dDevice->CreateInputLayout( layout, numElements, pVSBlob->GetBufferPointer(),
                                          pVSBlob->GetBufferSize(), &g_pVertexLayout );
    pVSBlob->Release();
    if( FAILED( hr ) )
        return hr;

    // Set the input layout
    g_pImmediateContext->IASetInputLayout( g_pVertexLayout );		

    // Set primitive topology
    g_pImmediateContext->IASetPrimitiveTopology( D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

 	//**************************************************************************//
	// Create the 3 constant buffers.											//
	//**************************************************************************//
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(CBMaterial);
    bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
    hr = g_pd3dDevice->CreateBuffer( &bd, NULL, &g_pCBMaterialBuffer );
    if( FAILED( hr ) )
        return hr;
    
    bd.ByteWidth = sizeof(CBChangeOnResize);
    hr = g_pd3dDevice->CreateBuffer( &bd, NULL, &g_pCBChangeOnResize );
    if( FAILED( hr ) )
        return hr;
    
    bd.ByteWidth = sizeof(CBChangesEveryFrame);
    hr = g_pd3dDevice->CreateBuffer( &bd, NULL, &g_pCBChangesEveryFrame );
    if( FAILED( hr ) )
        return hr; 

    // Create the sample state
    D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory( &sampDesc, sizeof(sampDesc) );
    sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD = 0;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

    hr = g_pd3dDevice->CreateSamplerState( &sampDesc, &g_pSamplerLinear );
    if( FAILED( hr ) )
        return hr; 

	g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);
	
	hr = InitLighting();

	if (FAILED(hr))
	{
		return hr;
	}

	g_input = new Input();
	g_input->InitialiseInput(g_hInst, g_hWnd);

	FollowCamera* camera = new FollowCamera();
	camera->SetInput(g_input);
	camera->SetPosition(0.0f, 8.0f, -50.0f);
	//camera->SetOffset(0.0f, 8.0f, -50.0f);
	camera->SetFieldOfView(1000.0f, 0.01f, width / (FLOAT) height, DirectX::XM_PIDIV4);
	g_camera = camera;

	g_particleSystem = new SimpleParticleSystem();
	hr = g_particleSystem->Initialise(g_pd3dDevice, g_pImmediateContext, g_pCBChangesEveryFrame);
	
	if (FAILED(hr))
	{
		return hr;
	}

	g_particleSystem->SetRenderCamera(g_camera);

	hr = InitBOIDS();

	if (FAILED(hr))
	{
		return hr;
	}

	CBChangeOnResize cbChangesOnResize;
    cbChangesOnResize.matProjection = XMMatrixTranspose( g_camera->GetProjectionMatrix() );
    g_pImmediateContext->UpdateSubresource( g_pCBChangeOnResize, 0, NULL, &cbChangesOnResize, 0, 0 );
	
	D3D11_RASTERIZER_DESC wfdesc;
	ZeroMemory(&wfdesc, sizeof(D3D11_RASTERIZER_DESC));
	wfdesc.CullMode = D3D11_CULL_BACK;
	wfdesc.FillMode = D3D11_FILL_SOLID;
	hr = g_pd3dDevice->CreateRasterizerState(&wfdesc, &g_pRasterizerState);

	g_pImmediateContext->RSSetState(g_pRasterizerState);		

	hr = InitObjects();

	if (FAILED(hr))
	{
		return hr;
	}

	hr = InitSkyBox();	

	if (FAILED(hr))
	{
		return hr;
	}

	g_timer = new Timer();
	g_timer->SetDriverType(g_driverType);

    return S_OK;
}

HRESULT InitSkyBox()
{
	HRESULT hr = S_OK;

	ID3DBlob* pSkyPixelBlob = NULL;
	
	//compile basic shader
	hr = ShaderHelper::CompileShaderFromFile(L"Skybox_PixelShaders.hlsl", "PS_SkyboxBasic", "ps_4_0", &pSkyPixelBlob);

	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	hr = g_pd3dDevice->CreatePixelShader(pSkyPixelBlob->GetBufferPointer(), pSkyPixelBlob->GetBufferSize(), NULL, &g_skyNoEffect);

#ifdef _DEBUG
	{
		const char name[] = "Sky no effect";
		g_skyNoEffect->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	//compile the wibble shader
	hr = ShaderHelper::CompileShaderFromFile(L"Skybox_PixelShaders.hlsl", "PS_SkyboxWibble", "ps_4_0", &pSkyPixelBlob);

	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	hr = g_pd3dDevice->CreatePixelShader(pSkyPixelBlob->GetBufferPointer(), pSkyPixelBlob->GetBufferSize(), NULL, &g_skyPixelWibble);

	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

#ifdef _DEBUG
	{
		const char name[] = "Sky pixel wibble";
		g_skyPixelWibble->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	//compile the colour shader
	hr = ShaderHelper::CompileShaderFromFile(L"Skybox_PixelShaders.hlsl", "PS_SkyboxColour", "ps_4_0", &pSkyPixelBlob);

	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	hr = g_pd3dDevice->CreatePixelShader(pSkyPixelBlob->GetBufferPointer(), pSkyPixelBlob->GetBufferSize(), NULL, &g_skyColourEffect);

#ifdef _DEBUG
	{
		const char name[] = "Sky colour effect";
		g_skyColourEffect->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	//compile the crawler shader
	hr = ShaderHelper::CompileShaderFromFile(L"Skybox_PixelShaders.hlsl", "PS_SkyboxCrawler", "ps_4_0", &pSkyPixelBlob);

	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	hr = g_pd3dDevice->CreatePixelShader(pSkyPixelBlob->GetBufferPointer(), pSkyPixelBlob->GetBufferSize(), NULL, &g_skyboxCrawler);
	
	pSkyPixelBlob->Release();

	if (FAILED(hr))
		return hr;

#ifdef _DEBUG
	{
		const char name[] = "Skybox crawler";
		g_skyboxCrawler->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	g_currentSkybox = g_skyboxCrawler;

	D3D11_BUFFER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	desc.ByteWidth = sizeof(CBSkybox);
	desc.CPUAccessFlags = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.MiscFlags = 0;
		
	hr = g_pd3dDevice->CreateBuffer(&desc, nullptr, &g_skyBoxBuffer);
	
	if (FAILED(hr))
		return hr;

#ifdef _DEBUG
	{
		const char name[] = "Skybox buffer";
		g_skyBoxBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(name) - 1, name);
	}
#endif

	g_skyBoxInfo.colour = DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	g_skyBoxInfo.time = 0.0f;
	//skybox.colour = XMFLOAT4(0.9f, 0.9f, 0.9f, 1.0f);

	g_pImmediateContext->UpdateSubresource(g_skyBoxBuffer, 0, nullptr, &g_skyBoxInfo, 0, 0);
	
	SimpleObject* cube = CreateSimpleObject("Media\\Textured_triangulated_Cube\\cube.obj", false);

	cube->SetScale(200.0f, 200.0f, 200.0f);

	g_skybox = cube;

	g_crawler = new Crawler();
	g_crawler->SetSkyBoxInfo(&g_skyBoxInfo);
	g_crawler->SetSky(g_skybox);

	return hr;
}

HRESULT InitObjects()
{
	HRESULT hr = S_OK;
	
	MoveableObject* deerOBJ = CreateMoveableObject("Media\\deer-obj\\deer-obj.obj", false);
	deerOBJ->SetSpeed(5.0f);
	g_theDeer = deerOBJ;
	g_boidsSimulation->SetAvoidObject(g_theDeer);
	//MoveableObject* deerOBJ = CreateMoveableObject("Media\\pig\\pig\\pig.obj", true);

	g_objects.push_back(deerOBJ);

	RotatingObject* cupOBJ = CreateRotatingObject("Media\\Cup\\cup.obj", true);

	cupOBJ->SetParent(deerOBJ);
	cupOBJ->SetPosition(-10.0f, 12.0f, -2.0f);
	cupOBJ->SetScale(4.0f, 4.0f, 4.0f);

	g_objects.push_back(cupOBJ);

	FollowCamera* camera = (FollowCamera*)g_camera;
	camera->SetTarget(deerOBJ);

	SimpleObject* pigOBJ = CreateSimpleObject("Media\\pig\\pig\\pig.obj", true);

	pigOBJ->SetPosition(8.0f, 5.0f, 0.0f);
	pigOBJ->SetScale(4.0f, 4.0f, 4.0f);
	g_objects.push_back(pigOBJ);
	
	return hr;
}

HRESULT InitBOIDS()
{
	HRESULT hr = S_OK;

	g_boidsSimulation = new SimpleBOIDS();
	g_boidsSimulation->SetRenderCamera(g_camera);
	g_boidsSimulation->SetMaterials(&g_materials);
	g_boidsSimulation->SetTextures(&g_textures);
	g_boidsSimulation->SetSampler(g_pSamplerLinear);

	Mesh* boidsMesh = LoadMesh("Media\\pig\\pig\\pig.obj", true);

	hr = g_boidsSimulation->Initialise(g_pd3dDevice, g_pImmediateContext, g_pCBChangesEveryFrame, g_pCBMaterialBuffer, boidsMesh);

	if (FAILED(hr))
	{
		return hr;
	}

	return hr;
}

HRESULT InitLighting()
{
	HRESULT hr = S_OK;

	g_lightingEngine = new SimpleLightingEngine();
	hr = g_lightingEngine->Initialise(g_pd3dDevice, g_pImmediateContext);
	
	if (FAILED(hr))
	{
		return hr;
	}

	g_testLight = g_lightingEngine->AddAndCreateLight();

	g_testLight->SetDiffuse(DirectX::XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
	g_testLight->SetRange(200.0f);
	g_testLight->SetAttenuation(DirectX::XMFLOAT3(0.4f, 0.02f, 0.0f));
	g_testLight->SetLightType(SpotLight);

	/*Light* light = g_lightingEngine->AddAndCreateLight();
	light->SetLightType(Directional);
	light->SetDiffuse(DirectX::XMFLOAT4(0.5f, 0.5f, 0.5f, 0.0f));
	light->SetDirection(DirectX::XMFLOAT3(-1.0f, 0.0f, 0.0f));*/

	g_lightingEngine->SetGlobalAmbient(DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f));

	return hr;
}

SimpleObject* CreateSimpleObject(LPSTR meshFilename, bool LHS)
{
	Mesh* mesh = LoadMesh(meshFilename, LHS);
	Renderer* renderer = new Renderer();

	renderer->Initialise(g_pd3dDevice, mesh, g_pCBChangesEveryFrame, g_pCBMaterialBuffer);
	renderer->SetMaterialResources(&g_materials);
	renderer->SetTextureResources(&g_textures);

	g_renderers.push_back(renderer);

	SimpleObject* newObject = new SimpleObject();	
	newObject->CreateObject(renderer);

	return newObject;
}

RotatingObject* CreateRotatingObject(LPSTR meshFilename, bool LHS)
{
	Mesh* mesh = LoadMesh(meshFilename, LHS);
	Renderer* renderer = new Renderer();

	renderer->Initialise(g_pd3dDevice, mesh, g_pCBChangesEveryFrame, g_pCBMaterialBuffer);
	renderer->SetMaterialResources(&g_materials);
	renderer->SetTextureResources(&g_textures);

	g_renderers.push_back(renderer);
	
	RotatingObject* newObject = new RotatingObject();	
	newObject->CreateObject(renderer);

	return newObject;
}

MoveableObject* CreateMoveableObject(LPSTR meshFilename, bool LHS)
{
	Mesh* mesh = LoadMesh(meshFilename, LHS);
	Renderer* renderer = new Renderer();

	renderer->Initialise(g_pd3dDevice, mesh, g_pCBChangesEveryFrame, g_pCBMaterialBuffer);
	renderer->SetMaterialResources(&g_materials);
	renderer->SetTextureResources(&g_textures);	

	g_renderers.push_back(renderer);	

	MoveableObject* newObject = new MoveableObject();
	newObject->SetInputManager(g_input);
	newObject->CreateObject(renderer);

	return newObject;
}

#define SAFE_RELEASE(p) if(p) {p->Release(); p=nullptr;}
#define SAFE_DELETE(p) if(p) {p->Release(); delete p;}

//--------------------------------------------------------------------------------------
// Clean up the objects we've created
//--------------------------------------------------------------------------------------
void CleanupDevice()
{
 	SAFE_RELEASE(g_pRasterizerState);
	SAFE_RELEASE(g_skyPixelWibble);
	SAFE_RELEASE(g_skyColourEffect);
	SAFE_RELEASE(g_skyNoEffect);
	SAFE_RELEASE(g_skyboxCrawler);
	SAFE_RELEASE(g_skyBoxBuffer);
	SAFE_DELETE(g_input);
	
	size_t size = g_renderers.size();

	for (size_t i = 0; i < size; ++i)
	{
		g_renderers[i]->Release();
		delete g_renderers[i];
	}

	size = g_materials.size();

	for (size_t i = 0; i < size; ++i)
	{
		delete g_materials[i];
	}	

	size = g_meshes.size();

	for (size_t i = 0; i < size; ++i)
	{
		g_meshes[i]->Release();
		delete g_meshes[i];
	}

	size = g_textures.size();

	for (size_t i = 0; i < size; ++i)
	{
		g_textures[i]->Release();
	}

	size = g_objects.size();

	for (size_t i = 0; i < size; ++i)
	{
		delete g_objects[i];
	}

	delete g_camera;
	delete g_timer;

	SAFE_DELETE(g_lightingEngine);
	SAFE_DELETE(g_particleSystem);
	SAFE_DELETE(g_boidsSimulation);

	delete g_crawler;	

	if (g_pImmediateContext) g_pImmediateContext->ClearState();

	if (g_pSamplerLinear) g_pSamplerLinear->Release();
	if (g_pCBMaterialBuffer) g_pCBMaterialBuffer->Release();
	if (g_pCBChangeOnResize) g_pCBChangeOnResize->Release();
	if (g_pCBChangesEveryFrame) g_pCBChangesEveryFrame->Release();
	if (g_pVertexLayout) g_pVertexLayout->Release();
	if (g_pVertexShader) g_pVertexShader->Release();
	if (g_pPixelShader) g_pPixelShader->Release();
	if (g_pDepthStencil) g_pDepthStencil->Release();
	if (g_pDepthStencilView) g_pDepthStencilView->Release();
	if (g_pRenderTargetView) g_pRenderTargetView->Release();
	if (g_pSwapChain) g_pSwapChain->Release();
	if (g_pImmediateContext) g_pImmediateContext->Release();

//#ifdef _DEBUG
//
//	ID3D11Debug* debugger = nullptr;
//	
//	g_pd3dDevice->QueryInterface(__uuidof(ID3D11Debug), (void**)(&debugger));
//
//	debugger->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
//
//	debugger->Release();
//
//#endif

	if (g_pd3dDevice) g_pd3dDevice->Release();
}

Mesh* LoadMesh(LPSTR meshFilename, bool LHS)
{
	MeshLoader loader;
	loader.Initialise(g_pd3dDevice, &g_materials, &g_meshes, &g_textures);

	Mesh* mesh = loader.LoadMesh(meshFilename, LHS);

	bool loaded = false;

	size_t size = g_meshes.size();

	for (size_t i = 0; i < size; i++)
	{
		if (g_meshes[i] == mesh)
		{
			loaded = true;
			break;
		}
	}

	if (!loaded)
	{
		g_meshes.push_back(mesh);
	}

	return mesh;
}

//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
    PAINTSTRUCT ps;
    HDC hdc;

    switch( message )
    {
        case WM_PAINT:
            hdc = BeginPaint( hWnd, &ps );
            EndPaint( hWnd, &ps );
            break;

        case WM_DESTROY:
            PostQuitMessage( 0 );
            break;

        default:
            return DefWindowProc( hWnd, message, wParam, lParam );
    }

    return 0;
}

void ScrollSkyBox()
{
	static float scroll = 120.0f;

	scroll += g_input->GetMouseWheel();

	//check scrolling
	if (scroll > 480.0f)
	{
		scroll = 120.0f;
	}
	else if (scroll == 480.0f)
	{
		g_currentSkybox = g_skyNoEffect;
	}
	else if (scroll == 360.0f)
	{
		g_currentSkybox = g_skyColourEffect;
	}
	else if (scroll == 240.0f)
	{
		g_currentSkybox = g_skyPixelWibble;
	}
	else if (scroll == 120.0f)
	{
		g_currentSkybox = g_skyboxCrawler;
	}
	else if (scroll <= 0.0f)
	{
		scroll = 480.0f;
	}

	//check keyboard presses
	if (g_input->KeyPressed(DIKEYBOARD_7))
	{
		g_currentSkybox = g_skyNoEffect;
		scroll = 480.0f;
	}
	else if (g_input->KeyPressed(DIKEYBOARD_8))
	{
		g_currentSkybox = g_skyPixelWibble;
		scroll = 240.0f;
	}
	else if (g_input->KeyPressed(DIKEYBOARD_9))
	{
		g_currentSkybox = g_skyColourEffect;
		scroll = 360.0f;
	}
	else if (g_input->KeyPressed(DIKEYBOARD_0))
	{
		g_currentSkybox = g_skyboxCrawler;
		scroll = 120.0f;
	}
}

void UpdateSkyBox()
{
	ScrollSkyBox();

	float t = (float)g_timer->GetFrameTime();
		
	g_skybox->SetPosition(g_camera->GetPosition().x, g_camera->GetPosition().y, g_camera->GetPosition().z);
	g_skybox->Update(t);

	if (g_currentSkybox == g_skyboxCrawler)
	{
		g_crawler->Update(t);
	}
}

void Update()
{
	g_input->Update();
	g_timer->Update();

	CheckExit();

	float t = (float)g_timer->GetFrameTime();

	g_camera->Update(t);	
	
	g_lightingEngine->SetEyePosition(g_camera->GetPosition());

	UpdateSkyBox();

	CheckSound();	

	size_t size = g_objects.size();

	for (size_t i = 0; i < size; i++)
	{
		g_objects[i]->Update(t);
	}
	
	UpdateLight();

	g_boidsSimulation->Update(t);
}

void RenderObjects()
{	
	DirectX::XMMATRIX matView = g_camera->GetViewMatrix();
	DirectX::XMMATRIX matProjection = g_camera->GetProjectionMatrix();

	g_pImmediateContext->IASetInputLayout(g_pVertexLayout);

	g_pImmediateContext->VSSetShader(g_pVertexShader, NULL, 0);

	g_pImmediateContext->PSSetShader(g_pPixelShader, NULL, 0);
	
	g_pImmediateContext->RSSetState(g_pRasterizerState);

	//int amount = 0;

	size_t size = g_objects.size();

	for (size_t i = 0; i < size; i++)
	{
		BoundingBox box = g_objects[i]->GetBoundingBox();

		if (!g_camera->CullObject(box))
		{
			g_objects[i]->Render(g_pImmediateContext, matProjection, matView);
			//amount++;
		}
	}

	////Uncomment to see culling in action
	/*std::string aString;

	aString = std::to_string(amount) + "\n";

	OutputDebugStringA(aString.c_str());*/
}

void RenderSkyBox()
{
	g_pImmediateContext->PSSetShader(g_currentSkybox, NULL, 0);

	DirectX::XMMATRIX matView = g_camera->GetViewMatrix();
	DirectX::XMMATRIX matProjection = g_camera->GetProjectionMatrix();

	g_skyBoxInfo.time = (float)g_timer->GetTime();	

	g_pImmediateContext->UpdateSubresource(g_skyBoxBuffer, 0, nullptr, &g_skyBoxInfo, 0, 0);

	g_pImmediateContext->PSSetConstantBuffers(5, 1, &g_skyBoxBuffer);

	g_skybox->Render(g_pImmediateContext, g_camera->GetProjectionMatrix(), matView);
}

//--------------------------------------------------------------------------------------
// Render a frame
//--------------------------------------------------------------------------------------
void Render()
{
	//
	// Clear the back buffer
	//
	float ClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f }; // red, green, blue, alpha
	g_pImmediateContext->ClearRenderTargetView(g_pRenderTargetView, ClearColor);

	//
	// Clear the depth buffer to 1.0 (max depth)
	//
	g_pImmediateContext->ClearDepthStencilView(g_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	g_pImmediateContext->IASetInputLayout(g_pVertexLayout);

	g_pImmediateContext->VSSetShader(g_pVertexShader, NULL, 0);
	
	//g_pImmediateContext->PSGetSamplers(0, 1, &g_pSamplerLinear);
	g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pCBChangeOnResize);	
	g_pImmediateContext->RSSetState(g_pRasterizerState);
	
	g_lightingEngine->Update();

	float t = (float)g_timer->GetFrameTime();	
	
	RenderSkyBox();

	g_particleSystem->Render(g_pImmediateContext, t);

	RenderObjects();

	g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);

	g_boidsSimulation->Render(g_pImmediateContext);

    //
    // Present our back buffer to our front buffer
    //
    g_pSwapChain->Present( 0, 0 );
}

void CheckSound()
{
	if (g_input->KeyPressed(DIKEYBOARD_E))
	{
		PlaySound(L"337911__fschaeffer__83bells.wav", NULL, SND_ASYNC);
	}
}

void CheckExit()
{
	bool escapePressed;

	escapePressed = g_input->GetKeyState(DIKEYBOARD_ESCAPE);
	if (escapePressed)
	{
		int choice = MessageBox(0, L"Exit the application?", L"Exit Application", MB_YESNO);

		if (choice == IDYES)
		{
			PostMessage(g_hWnd, WM_DESTROY, 0, 0);
		}
	}
}

void UpdateLight()
{
	using namespace DirectX;

	XMFLOAT3 rotation = g_theDeer->GetRotation();

	rotation.y -= XM_PIDIV2;

	const float x = cosf(rotation.x) * sinf(rotation.y);
	const float y = -sinf(rotation.x);
	const float z = cosf(rotation.x) * cosf(rotation.y);
		
	rotation = XMFLOAT3(x, y, z);

	g_testLight->SetDirection(rotation);

	static float lightCone = DirectX::XMConvertToRadians(45.0f);

	if (g_input->GetKeyState(DIKEYBOARD_UP))
	{
		lightCone += (float)g_timer->GetFrameTime() * 0.2f;
	}
	else if (g_input->GetKeyState(DIKEYBOARD_DOWN))
	{
		lightCone -= (float)g_timer->GetFrameTime() * 0.2f;
	}

	g_testLight->SetConeAngle(lightCone < 0.0f ? 0.0f : lightCone);

	XMFLOAT3 position = g_theDeer->GetPosition();
	position.y += 5.0f;
	g_testLight->SetPosition(position);

	if (g_input->KeyPressed(DIKEYBOARD_1))
	{
		g_testLight->SetLightType(Directional);
	}
	else if (g_input->KeyPressed(DIKEYBOARD_2))
	{
		g_testLight->SetLightType(Point);
	}
	else if (g_input->KeyPressed(DIKEYBOARD_3))
	{
		g_testLight->SetLightType(SpotLight);
	}

	if (g_input->KeyPressed(DIKEYBOARD_TAB))
	{
		g_testLight->SetEnabled(!g_testLight->GetEnabled());
	}
}

//**************************************************************************//
// Convert an old chracter (char *) string to a WCHAR * string.  There must//
// be something built into Visual Studio to do this for me, but I can't		//
// find it - Nigel.															//
//**************************************************************************//
void charStrToWideChar(WCHAR *dest, char *source)
{
	int length = (int)strlen(source);
	for (int i = 0; i <= length; i++)
		dest[i] = (WCHAR) source[i];
}