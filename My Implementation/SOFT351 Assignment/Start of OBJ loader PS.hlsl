//**************************************************************************//
// Start of an OBJ loader.  By no means an end.  This just creates			//
// triangles.																//
//**************************************************************************//

//**************************************************************************//
// Modifications to the MS sample code is copyright of Dr Nigel Barlow,		//
// lecturer in computing, University of Plymouth, UK.						//
// email: nigel@soc.plymouth.ac.uk.											//
//																			//
// You may use, modify and distribute this (rather cack-handed in places)	//
// code subject to the following conditions:								//
//																			//
//	1:	You may not use it, or sell it, or use it in any adapted form for	//
//		financial gain, without my written premission.						//
//																			//
//	2:	You must not remove the copyright messages.							//
//																			//
//	3:	You should correct at least 10% of the typig abd spekking errirs.   //
//**************************************************************************//
//--------------------------------------------------------------------------------------
// File: Tutorial07 - Textures and Constant buffers.fx
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------

#define MAX_NUMBER_LIGHTS 8

#define DIRECTIONAL 0
#define POINT 1
#define SPOT 2

struct Light
{
	float3 direction;	
	float cone;	
	float4 diffuseCol;
	float3 position;
	float range;
	float3 attenuation;
	int lightType;
	bool enabled;
	float3 padding;
};

struct LResult
{
	float4 diffuse;
	float4 specular;
};

//**************************************************************************//
// Constant Buffer Variables.												//
//**************************************************************************//
cbuffer cbMaterial : register( b0 )
{
    float4 materialColour;
	float4 ambientColour;
	float4 materialSpecular;
	float specularPower;
	bool hasTexture;
	bool hasBumpMap;
	float padding;
};

cbuffer cbLightingProperties : register(b3)
{
	float4 globalAmbience;

	float4 eyePosition;
	
	Light sceneLights[MAX_NUMBER_LIGHTS];

	int numberLights;
	float3 paddingStuff;
};

cbuffer cbParticles : register(b4)
{
	float4 particleColour;
}

//**************************************************************************//
// Textures and Texture Samplers.  These variables can't (it seems) go in	//
// constant buffers; I think the size of a CB is too restricted.			//
//																			//
// We only have one Texture2D variable here, what does that suggest about	//
// the nature or our mesh, i.e. could it be multi-textured?					//
//**************************************************************************//
Texture2D    txDiffuse : register( t0 );
Texture2D    txBumpMap : register( t1 );
SamplerState samLinear : register( s0 );

//**************************************************************************//
// Pixel shader input structure.	The semantics (the things after the		//
// colon) look a little weird.  The semantics are used (so Microsoft tell	//
// us) used by the compiler to link shader inputs and outputs. 				//
//																			//
// For this to work, you must ensure this structure is identical to the		//
// vertex shader's output structure.										//
//**************************************************************************//
struct PS_INPUT
{
	float4 Pos		 : SV_POSITION;
	float4 WorldPos  : POSITION;
	float3 vecNormal : NORMAL;
	float3 tangent   : TANGENT;
	float2 Tex		 : TEXCOORD;
};

float4 PS_Particles(PS_INPUT input) : SV_Target
{
	float4 diffuse;
	
	diffuse = particleColour;

	return diffuse;
}

//**************************************************************************//
// Pixel Shader.	This one has no lighting.  You can have many pixel		//
// shaders in one of these files.											//
//**************************************************************************//
float4 PS_TexturesNoLighting( PS_INPUT input) : SV_Target
{
	float4 diffuse = materialColour;

	if (hasTexture)
		diffuse = diffuse * txDiffuse.Sample(samLinear, input.Tex);

    return diffuse;
}

float GetAttenuation(Light light, float distance)
{
	float attenuation;

	//attenuation[0] = constant modifier, applies a minimum amount of light
	//attenuation[1] = linear modifier, applies light inversely proportional to the distance
	//attenuation[2] = exponential modifier, modifies light again, used to help with falloff
	attenuation = light.attenuation[0] + (light.attenuation[1] * distance) + (light.attenuation[2] * (distance * distance));

	attenuation = 1 / attenuation;

	return attenuation;
}

float4 GetDiffuse(float4 lightColour, float3 direction, float3 normal)
{
	//get the dot product of the light, if its less than zero then no lighting is applied
	float dotLight = max(0, dot(normal, direction));

	return lightColour * dotLight;
}

float4 GetSpecular(Light light, float3 viewVector, float3 lightVec, float3 normal)
{
	////Doesn't work properly for some odd reason, its probably too good
	////Get reflection vector
	//float3 reflecVec = normalize(reflect(-lightVec, normal));

	////get the dot product of the normal and reflection vector
	//float reflecDot = saturate(max(0.0f, dot(reflecVec, viewVector)));

	////get the final specular shading
	//float4 spec = light.diffuseCol * pow(reflecDot, specularPower);

	//float4 finalSpec = saturate(spec);

	//Get the half vector
	float3 halfVec = normalize(lightVec + viewVector) / 2;

	//get the dot product of the normal and half vector
	float normalDot = max(0.0f, dot(normal, halfVec));

	//get the final specular shading
	float4 finalSpec = light.diffuseCol * pow(normalDot, specularPower);

	return finalSpec;
}

LResult DoDirectionalLight(Light light, float4 position, float3 normal)
{
	LResult result;

	//get the light vector
	//from the point being shaded to the light source
	float3 lightVec = -light.direction;

	//Get the diffuse colour for the shading
	result.diffuse = GetDiffuse(light.diffuseCol, lightVec, normal);
	result.specular = GetSpecular(light, eyePosition, lightVec, normal);

	return result;
}

LResult DoPointLight(Light light, float4 position, float3 normal)
{
	LResult result;

	float4 finalColour = {0,0,0,0};

	//get the light vector
	//from the point being shaded to the light source
	float3 lightVec = light.position - position;

	//get the distance from the lights source to the position
	float distance = length(lightVec);
	
	if (distance > light.range)
	{
		result.diffuse = float4(0,0,0,0);
		result.specular = float4(0,0,0,0);
		
		return result;
	}

	//normalise the light vector
	lightVec /= distance;

	//get the attenuation
	float attenuation = GetAttenuation(light, distance);

	finalColour = GetDiffuse(light.diffuseCol, lightVec, normal);

	finalColour *= attenuation;

	result.diffuse = finalColour;
	result.specular = GetSpecular(light, eyePosition, lightVec, normal) * attenuation;

	return result;
}

float GetSpotCone(Light light, float3 lightDirection)
{
	float minCos = cos(light.cone);
	float maxCos = (minCos + 1.0f) / 2.0f;
	float cosAngle = dot(light.direction, -lightDirection);
	return smoothstep(minCos, maxCos, cosAngle);	

	//an alternative approach to cone intensity, larger cone value, the more focused the spotlight is
	//return pow(max(dot(-lightDirection, light.direction), 0.0f), light.cone);
}

LResult DoSpotLight(Light light, float4 position, float3 normal)
{
	LResult result;

	float4 finalColour = {0,0,0,0};

	//get the light vector
	//from the point being shaded to the light source
	float3 lightVec = light.position - position;

	//get the distance from the lights source to the position
	float distance = length(lightVec);

	if (distance > light.range)
	{
		result.diffuse = float4(0, 0, 0, 0);
		result.specular = float4(0, 0, 0, 0);

		return result;
	}

	//normalise the light vector
	lightVec /= distance;

	//get the attenuation
	float attenuation = GetAttenuation(light, distance);

	//Get the cones intensity
	float coneIntensity = GetSpotCone(light, lightVec);

	finalColour = GetDiffuse(light.diffuseCol, lightVec, normal);	

	finalColour *= attenuation;
	finalColour *= coneIntensity;	

	result.diffuse = finalColour;
	result.specular = GetSpecular(light, eyePosition, lightVec, normal);

	return result;
}

LResult CalculateLighting(float4 position, float3 normal)
{
	LResult result;

	for (int i = 0; i < numberLights; i++)
	{
		Light light = sceneLights[i];
		LResult current;

		if (!light.enabled)
		{
			continue;
		}

		switch (light.lightType)
		{
			case DIRECTIONAL:
			{
				current = DoDirectionalLight(light, position, normal);
			}
			break;
			case POINT:
			{
				current = DoPointLight(light, position, normal);
			}
			break;
			case SPOT:
			{
				current = DoSpotLight(light, position, normal);
			}
			break;
		}

		result.diffuse += current.diffuse;
		result.specular += current.specular;
	}

	result.diffuse = saturate(result.diffuse);
	result.specular = saturate(result.specular);

	return result;		
}

float4 PS_TexturesWithLighting( PS_INPUT input) : SV_Target
{
	//float4 finalColour = CalculateLighting(input.WorldPos, input.vecNormal);
	
	float4 finalColour = {0,0,0,0};

//bump map stuff
	if (hasBumpMap)
	{
		float4 normalMap = txBumpMap.Sample(samLinear, input.Tex);

		//bring into the -1 to 1 range
		normalMap = (2.0f * normalMap) - 1.0f;

		//make the tangent orthoganal to the normal, 90 degrees, the tangent is the V of our UV's
		input.tangent = normalize(input.tangent - dot(input.tangent, input.vecNormal) * input.vecNormal);

		//grab the bi tangent,the u in our UV's
		float3 biTangent = cross(input.vecNormal, input.tangent);

		//create the texture space
		float3x3 texSpace = float3x3(input.tangent, biTangent, input.vecNormal);

		//convert our normal map value into the texture space, 
		//the lighting calculations are now done using this normal as opposed to the original input normal, the interpolated normal
		input.vecNormal = normalize(mul(normalMap, texSpace));
	}

	LResult result = CalculateLighting(input.WorldPos, input.vecNormal);

	float4 ambient = ambientColour * globalAmbience;
	float4 diffuse = result.diffuse * materialColour;
	float4 specular = result.specular * materialSpecular;
	
	finalColour = ambient + diffuse + specular;

	if (hasTexture)
	{
		finalColour *= txDiffuse.Sample(samLinear,input.Tex);
	}	

	finalColour = saturate(finalColour);	
	
	return finalColour;
}

//TODO: keep working on the shader
//TODO: sort out sine wave stuff, then see about little sky box crawler