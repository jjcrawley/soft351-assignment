#include "Timer.h"
#include <DirectXMath.h>

Timer::Timer()
{
	m_timeStart = GetTickCount();
	m_previousTime = m_timeStart;
}

double Timer::GetTime()
{
	return (double) m_timeSinceStartup;
}

double Timer::GetFrameTime()
{
	return m_deltaTime;
}

void Timer::Update()
{
	//**************************************************************************//
	// Update our time.  This block is supposed to make the movement frame rate //
	// independent, as the frame rate we get depends of the performance of our	//
	// computer.  We may even be in reference (software emulation) mode, which	//
	// is painfully slow.														//
	//**************************************************************************//
	if (m_driverType == D3D_DRIVER_TYPE_REFERENCE)
	{
		m_timeSinceStartup += (DWORD)m_deltaTime;		
	}
	else
	{		
		DWORD currentTime = GetTickCount();

		m_deltaTime = (currentTime - m_previousTime) / 1000.0f;

		m_timeSinceStartup = currentTime - m_timeStart;

		m_previousTime = currentTime;
	}

	/*std::string aString = std::to_string(m_deltaTime) + " " + std::to_string(m_timeStart) + "\n";
	OutputDebugStringA(aString.c_str());*/
}

void Timer::SetDriverType(D3D_DRIVER_TYPE type)
{
	m_driverType = type;

	if (m_driverType == D3D_DRIVER_TYPE_REFERENCE)
	{
		m_deltaTime = DirectX::XM_PI * 0.0125f;
	}
}