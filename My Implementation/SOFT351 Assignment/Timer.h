#pragma once
//#include <d3d9.h>
//#include <d3d10.h>
#include <d3d11.h>

#ifndef TIMER
#define TIMER
class Timer
{
public:
	Timer();
	double GetTime();
	double GetFrameTime();
	void Update();
	void SetDriverType(D3D_DRIVER_TYPE type);
private:
	DWORD m_timeStart = 0;		
	DWORD m_timeSinceStartup = 0;
	float m_deltaTime = 0;
	DWORD m_previousTime = 0;
	D3D_DRIVER_TYPE m_driverType;
};
#endif