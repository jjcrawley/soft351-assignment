This repository contains my SOFT351 module code for a simple C++ directx11 game framework.

The framework was built off the code in the Original Code folder, which is included for comparison purposes.

The My Implementation folder contains the code that was submitted for the final assignment of the module.
The folder also contains two reports which were written for each assignment providing a basic overview and reflection of the code written for each part.

To Build:
You'll need to have Visual Studio 2015 installed to build and run.
It also requires a 64 bit installation of Windows 10.

The latest build can be found here: https://drive.google.com/open?id=1j98p-1oFcBUsJy_lh3hLcN9GUpAJmNPn

If you have any questions then feel free to contact me.
My email is joshcrawley18@yahoo.co.uk
My site is https://joshcrawley18.wixsite.com/joshuacrawley 